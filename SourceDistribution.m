function [la_s,th_s] = SourceDistribution(s)


if s.dipole
    
    la_s = (s.la_0-180)*pi/180; th_s = s.th_0*pi/180; % single source

else
    
    [~, name, ~] = fileparts(s.SourceFile);
    SourceName = ['source/source_distrib_',char(name),'_month_',num2str(s.month),'_UT_',num2str(s.UT),'.mat'];

    if ~exist(SourceName,'file')
       
        fprintf('\nComputing source distribution...... ') ;

        [tcu,~,lat,lon] = readSourceFile(s.coarsen_box,s.SourceFile);
        [la_s,th_s] = generateSourcePoints(lon,lat,tcu(:,:,s.month,s.UT));
        la_s = la_s*pi/180; th_s = th_s*pi/180;
        
        save(SourceName,'la_s','th_s')
    
    else
        
        fprintf('\nLoading source distribution...... ')       
        load(SourceName,'la_s','th_s')
        fprintf('done.\n') ;
        
    end
    
end
    
return