function [u,Rt,Is,Itot,Ipos,V] = GECsolver(NodeDistrib,D,Dp,Dr,source,sigma,analyticCond)

%--------------------------------------------------------------------------
% GECsolver - solve the GEC problem 
%
% INPUT:
%
%   NodeDistrib - name of the file containing the node distribution
%   D - Diff. matrix of GEC problem
%   Dp - Diff matrix of simplified GEC preconditioner problem 
%   Dr - Diff. matrix of radial derivative 
%   source - column vector containing the source term of GEC problem 
%   sigma - column vector containing the GEC conductivity 
%   analyticCond - flag to set up analytical or numerical conductivity
%
%
% OUTPUT:
%
%   u - electric potential (V):
%       -> first column, whole GEC solution (Rt*Is*uf+us)
%       -> second column, fair weather potential (Rt*Is*uf) (second column) 
%       -> third column, source potential (us)
%
%   Rt - atmospheric resistance (Ohms)
%   Is - current (A) at top boundary due to sources
%   Itot - net current (A) at top boundary (must be conserved ~0)
%   Ipos - positive current (A) at top boundary (Itot = Ipos + Ineg ~0)
%   V - Potential (V) at top boundary (V=Rt*Is)
%
%--------------------------------------------------------------------------


fprintf('\nComputing GEC solution......\n') ;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GEC SOLVER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load(NodeDistrib,'bdry_bot','bdry_top','N','wghts','rf','x','y','z','e0')

%--------------------------------------------------------------------------
% Iterative solver
%--------------------------------------------------------------------------

%%% rhs
f = source./sigma;


%%% First, compute without sources, 1 volt at top boundary and 0 volt at bottom boundary
u = zeros(N,1);
aux = setdiff(1:N,[bdry_bot bdry_top]);

u(bdry_bot) = 0;
u(bdry_top) = 1;
rhs = -D(aux,[bdry_bot bdry_top])*u([bdry_bot bdry_top]);


%Reordering & precond. Diff. matrix
A = D(aux,aux);
o = symrcm(A);
[~,os] = sort(o);
Li = A(o,o);
Lp = Dp(o,o);
if analyticCond
    [L,U] = ilu(Li);
end


for i = 1:2
    
    if analyticCond
        tic, [ui,flag,relres,iter,resvec] = gmres(Li,rhs(o),20,1e-9,[],L,U); time = toc;
    else
        rhs = rhs(o);
        disp('-----------------------------------------------------------')
        tic, [ui,flag,relres,iter,resvec] = gmres(Li,rhs,20,1e-9,[],@(rhs)precond(Lp,rhs)); time = toc;
    end
    
    u(aux) = ui(os);

    disp('-----------------------------------------------------------')
    disp(['Elapsed time is ',num2str(time),' seconds.'])
    disp(['flag GMRES = ', num2str(flag),'; # iterations = ', num2str(iter),'; residual = ', num2str(relres),'.'])
    
    figure(10+i), semilogy(resvec), 
    xlabel('# iterations'), ylabel('residual')
    drawnow
    
    %%% Compute vertical electric field [V/km] and vertical current [A/km^2]
    Er   =-Dr*u;
    Jr   = sigma.*Er;
    Jr_t = Jr(bdry_top);
    
    if i==1
        % Rt is the total resistance of the domain
        % Set all bdrys at zero volts & introduce sources        
        uf = u;
        Rt = 1/-(wghts*Jr_t*rf^2);
        
        u(bdry_bot) = 0;
        u(bdry_top) = 0;
        rhs = -f(aux)-D(aux,[bdry_bot bdry_top])*u([bdry_bot bdry_top]);
    elseif i==2
        % Find current at top bdry
        us = u;
        Is = wghts*Jr_t*rf^2;
        
        [~,~,e] = cart2sph(x,y,z);
        ind = find(abs(e-(e0+0.025*42))<0.025/2);
    end
    
end

u = [Rt*Is*uf+us Rt*Is*uf us];

Er   =-Dr*u(:,1);
Jr   = sigma.*Er;
Jr_t = Jr(bdry_top);

% total current at top bdry
Itot   = wghts*Jr_t*rf^2;

% positive current at top bdry
gg   = find(Jr_t>0);
Ipos = wghts(gg)*Jr_t(gg)*rf^2;

% potential top bdry
V = Rt*Is;

fprintf('-----------------------------------------------------------\n')

fprintf('\nIntegrated quantities:\n') ;
disp('-----------------------------------------------------------')
disp(['Global Resistance = ', num2str(Rt),' Ohms.'])
disp(['Source Current at top boundary = ',num2str(Is),' A.'])
disp(['Net current at top boundary = ',num2str(Itot),' A.'])
disp(['Positive current at top boundary = ',num2str(Ipos),' A.'])
disp(['Electric Potential at top boundary = ', num2str(V),' V.'])
disp('-----------------------------------------------------------')

end