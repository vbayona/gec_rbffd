function [xx,yy,zz]=repelf3D(x,y,z,int,itermax,P)

Nint=length(int); 
%----------------------
n = 30;
K = P*1e-5;
[index,~]=knnsearch([x y z],[x(int) y(int) z(int)],'K',400);
%----------------------

iter = 1;
while iter<itermax  
    for s=1:Nint
        
        d2=(x(int(s))-x(index(s,:))).^2 + (y(int(s))-y(index(s,:))).^2 + (z(int(s))-z(index(s,:))).^2;
        [~,ind]=sort(d2);
        auxd=ind(2:n);
        
        x(int(s)) = x(int(s)) + K*sum((x(int(s))-x(index(s,auxd)))./d2(auxd).^1.5);
        y(int(s)) = y(int(s)) + K*sum((y(int(s))-y(index(s,auxd)))./d2(auxd).^1.5);
        z(int(s)) = z(int(s)) + K*sum((z(int(s))-z(index(s,auxd)))./d2(auxd).^1.5); 
        
    end   
    iter=iter+1;         
%     plot3(x(int),y(int),z(int),'ro')
%     axis equal
%     drawnow
end


xx=x(int);
yy=y(int);
zz=z(int);

return