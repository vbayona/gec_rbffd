function Q = ComputeCharge(NodeDistrib,u,Dr,Dlam,Dthe,sigma)

%--------------------------------------------------------------------------
% ComputeFields - Computes net charge from GEC solution
%
% INPUT:
%
%   NodeDistrib - name of the node distribution to be loaded
%   u - electric potential
%   Dr - Diff. matrix for radial derivative 
%   Dlam - Diff. matrix for longitudinal derivative 
%   Dthe - Diff. matrix for latitudinal derivative 
%   sigma - column vector containing the GEC conductivity 
%
% OUTPUT:
%
%   Q - net charge (C)
%
%--------------------------------------------------------------------------

load(NodeDistrib,'indomain')

ur = sigma.*(Dr*u(:,1));
ulam = sigma.*(Dlam*u(:,1));
uthe = sigma.*(Dthe*u(:,1));

S = -(Dr*ur + Dlam*ulam + Dthe*uthe);
Q = sum(S(indomain))/length(indomain);

disp('--------------------------------------------------------')
disp(['Total Charge = ', num2str(Q),' C.'])
disp('--------------------------------------------------------')

end