function [lsig_lam,lsig_the,lsig_r] = PieceInterp(e0,h,Nrep,xlayer,ylayer,zlayer,x,y,z,log_sig_lam,log_sig_the,log_sig_r)

[~,~,rlayer] = cart2sph(xlayer,ylayer,zlayer);
[~,~,r] = cart2sph(x,y,z);

lsig_lam = zeros(size(x));
lsig_the = zeros(size(x));
lsig_r   = zeros(size(x));

T=10;
Nr = ceil(Nrep/T);

for j=1:Nr+1
    
    indl = find((rlayer>e0+(j-1)*T*h-5*h).*(rlayer<=e0+j*T*h+5*h));
    ind  = find((r>e0+(j-1)*T*h-1.5*h).*(r<=e0+j*T*h+1.5*h));
    
    F_log_sig_lam = scatteredInterpolant(xlayer(indl),ylayer(indl),zlayer(indl),log_sig_lam(indl));
    lsig_lam(ind) = F_log_sig_lam(x(ind),y(ind),z(ind));
    clear F_log_sig_lam

    
    F_log_sig_the = scatteredInterpolant(xlayer(indl),ylayer(indl),zlayer(indl),log_sig_the(indl));
    lsig_the(ind) = F_log_sig_the(x(ind),y(ind),z(ind));
    clear F_log_sig_the

    
    F_log_sig_r   = scatteredInterpolant(xlayer(indl),ylayer(indl),zlayer(indl),log_sig_r(indl));
    lsig_r(ind)   = F_log_sig_r(x(ind),y(ind),z(ind));    
    clear F_log_sig_r
    
end

return