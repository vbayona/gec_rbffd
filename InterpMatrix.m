function [D,X,int,ext] = InterpMatrix(NodeDistrib,save_InterpMatrix)

%--------------------------------------------------------------------------
% InterpMatrix - computes the interpolation matrix for plotGEC.m function 
%
% INPUT:
%
%   NodeDistrib - name of the file containing the node distribution
%   save_DiffMatrix - flag to save GEC Diff. Matrices
%                     If it is set to 1, they are stored in a "*.mat" file,
%                     in "diff_matrix" directory.
%                     If it is set to 0, they are not stored.
%
% OUTPUT:
%
%   D  - Interpolation matrix
%   X  - Cartesian grid on which matrix D interpolates.
%   int - label for interior nodes
%   ext - label for exterior nodes
%
%--------------------------------------------------------------------------

fprintf('\nComputing interpolation matrix...... ') ;

%% inputs
%-----------------------------------------------------------------------
h = 0.05; n = 80; ep = 4.25;
%---------------------------
load(NodeDistrib,'E','Fb','x','y','z','r0','rf','hg','Nr','Beta')
%---------------------------
% GA-RBF function
%---------------------------
phi = @(r2,ep) sqrt(1+ep^2*r2);
%phi = @(r2,ep) exp(-ep^2*r2);
%---------------------------
% Evaluation nodes
%---------------------------
N  = round(max(x)-min(x))/h;
h  = (max(x)-min(x))/N;
xg = min(x)-2*h:h:max(x)+2*h;
[Xg,Yg,Zg] = meshgrid(xg,xg,xg);
xg=Xg(:); yg=Yg(:); zg=Zg(:);
X=[xg yg zg];
%---------------------------
[la,th,r] = cart2sph(xg,yg,zg);
int = find((r>=Fb(la,th)).*(r<E(rf)))';
ext = [find(r<Fb(la,th)); find(r>=E(rf))]';
%---------------------------
% Distance between nodes
%---------------------------
[index,~]=knnsearch([x' y' z'],X,'K',n);
%-----------------------------------------------------------------------


%% Interpolation Matrix
%-----------------------------------------------------------------------
N  = length(x);
Nd = length(int);
%---------------------------
ind_aux=zeros(n,Nd);
W=zeros(n,Nd);
%---------------------------
s = 1;
for k=1:Nd
    ii=int(k);
    ind = index(ii,:);
    ind_aux(:,s) = ind;
    %---------------------------
    [xx, xc] = meshgrid(x(ind));
    [yy, yc] = meshgrid(y(ind));
    [zz, zc] = meshgrid(z(ind));
    r2 = (xx-xc).^2 + (yy-yc).^2 + (zz-zc).^2;
    %---------------------------
    A   = phi(r2,ep);
    r2e = (x(ind)-xg(ii)).^2+(y(ind)-yg(ii)).^2+(z(ind)-zg(ii)).^2;
    b   = phi(r2e,ep)';
    W(:,s) = A\b;
    %---------------------------
    s=s+1;
end
%---------------------------
I = repmat(1:Nd,n,1);
D = sparse(I,ind_aux,W,Nd,N);
%-----------------------------------------------------------------------


if save_InterpMatrix
    
    % Save interp. matrix
    interpMatrix = strcat('diff_matrix/IM_',NodeDistrib);    
    save(interpMatrix,'D','X','int','ext')
    
end

fprintf('done.\n') ;

return
