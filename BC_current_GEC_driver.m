%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GEC_driver: driver to compute the GEC solution by enforcing the TRMM data 
% as a Neumann Boundary Condition at 20km.
%--------------------------------------------------------------------------

[g,c,s] = GEC_settings;

NodeDistrib = NodeGenerator(g);

[sigma,lsig_lam,lsig_the,lsig_r,F_sigma] = conductivity(NodeDistrib,g.nodesType,c,s,1);

[D,Dp,Dlam,Dthe,Dr] = DiffMatrix(NodeDistrib,lsig_lam,lsig_the,lsig_r,g.nodesType,1);

[uf,us,Rt,Is,Itot,Ipos,V] = BC_current_GECsolver(NodeDistrib,D,Dp,Dr,s,sigma,F_sigma,g.nodesType,c.analyticCond);

[E,J] = ComputeFields(uf,Dlam,Dthe,Dr,sigma);

Q = ComputeCharge(NodeDistrib,uf,Dr,Dlam,Dthe,sigma);

%--------------------------------------------------------------------------