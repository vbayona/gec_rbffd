%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GEC_driver: driver to compute the GEC solution using a dipole approach
%--------------------------------------------------------------------------

[g,c,s] = GEC_settings;

NodeDistrib = NodeGenerator(g);

[sigma,lsig_lam,lsig_the,lsig_r] = conductivity(NodeDistrib,g.nodesType,c,s,0);

[D,Dp,Dlam,Dthe,Dr] = DiffMatrix(NodeDistrib,lsig_lam,lsig_the,lsig_r,g.nodesType,0);

source = Source(NodeDistrib,s,g.Topo);

[u,Rt,Is,Itot,Ipos,V] = GECsolver(NodeDistrib,D,Dp,Dr,source,sigma,c.analyticCond);

[E,J] = ComputeFields(u(:,1),Dlam,Dthe,Dr,sigma);

Q = ComputeCharge(NodeDistrib,u,Dr,Dlam,Dthe,sigma);

%--------------------------------------------------------------------------