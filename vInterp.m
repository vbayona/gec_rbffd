function [r,S,h] = vInterp(NodeDistrib,X,Y)

%-----------------------------------------------------------------------
% Interpolate variable Y at location X = [long,lat] (in degrees).
%
% INPUT:
%
%   NodeDistrib - name of the file containing the node distribution
%   X - position [long,lat] in degrees for the vertical interpolation.
%   Y - variable to be interpolated as a function of the altitud, e.x E(:,2) (Elat)
%
% OUTPUT:
%
%   r - radial coordinate
%   S - interpolant
%   h - figure number
%
%-----------------------------------------------------------------------

load(NodeDistrib,'x','y','z','R','Fb','ef','r0','Nr')
%-----------------------------------------------------------------------
X = X*pi/180-[pi 0]; 
[la,th,r]=cart2sph(x',y',z');
[index,~] = knnsearch([la th],X,'K',50*Nr);
F = scatteredInterpolant(la(index),th(index),r(index),Y(index));
N = 200;
r = linspace(Fb(X(1),X(2)),ef,N)';
S = F(X(1)*ones(N,1),X(2)*ones(N,1),r);
r = R(r)-r0;
%-----------------------------------------------------------------------
h = figure; semilogx(abs(S),r);

return