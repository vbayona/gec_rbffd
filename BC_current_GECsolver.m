function [uf,us,Rt,Is,Itot,Ipos,V] = BC_current_GECsolver(NodeDistrib,D,Dp,Dr,s,sigma,F_sigma,nodesType,analyticCond)

%--------------------------------------------------------------------------
% BC_current_GECsolver - solve the GEC problem. It calls BC_current.m, which
% computes the source potential (us) by enforcing the TRMM data
% as a Neumann Boundary Condition at 20km. Then, computes the fair weather
% potential.
%
% INPUT:
%
%   NodeDistrib - name of the file containing the node distribution
%   D - Diff. matrix of GEC problem
%   Dp - Diff matrix of simplified GEC preconditioner problem 
%   Dr - Diff. matrix of radial derivative 
%   s - string defining source term.
%   sigma - column vector containing the GEC conductivity 
%   F_sigma - conductivity interpolant from given data to "NodeDistrib".
%   nodesType - type of nodes used (defined in "g" string)
%   analyticCond - flag to set up analytical or numerical conductivity
%
% OUTPUT:
%
%   uf - electric potential (V) in the fair weather 
%   us - electric potential (V) due to sources 
%   Rt - atmospheric resistance (Ohms)
%   Is - current (A) at top boundary due to sources
%   Itot - net current (A) at top boundary (must be conserved ~0)
%   Ipos - positive current (A) at top boundary (Itot = Ipos + Ineg ~0)
%   V - Potential (V) at top boundary (V=Rt*Is)
%
%--------------------------------------------------------------------------


fprintf('\nComputing GEC solution:\n') ;

%--------------------------------------------------------------------------
% Computing potential: source as BC
%--------------------------------------------------------------------------
[us,Jr_s,Is] = BC_current(NodeDistrib,s,F_sigma,nodesType,analyticCond);
%--------------------------------------------------------------------------

fprintf('\nFair weather potential......\n') ;
load(NodeDistrib,'bdry_bot','bdry_top','N','wghts','rf')

%--------------------------------------------------------------------------
% Iterative solver
%--------------------------------------------------------------------------

%First, compute without sources, 1 volt at top bdry and 0 volt at bottom bdry
uf = zeros(N,1);
aux = setdiff(1:N,[bdry_bot bdry_top]);

uf(bdry_bot) = 0;
uf(bdry_top) = 1;
rhs = -D(aux,[bdry_bot bdry_top])*uf([bdry_bot bdry_top]);

%--------------------------------------------------------------------------
%Reordering & precond. Diff. matrix
A = D(aux,aux);
o = symrcm(A);
[~,os] = sort(o);
Li = A(o,o);
Lp = Dp(o,o);
  
if analyticCond
    [L,U] = ilu(Li);
    tic, [ui,flag,relres,iter,resvec] = gmres(Li,rhs(o),20,1e-9,[],L,U); time = toc;
else
    rhs = rhs(o);
    disp('-----------------------------------------------------------')
    tic, [ui,flag,relres,iter,resvec] = gmres(Li,rhs,20,1e-9,[],@(rhs)precond(Lp,rhs)); time = toc;
end

uf(aux) = ui(os);

fprintf('done.\n') ;
disp('-----------------------------------------------------------')
disp(['Elapsed time is ',num2str(time),' seconds.'])
disp(['flag GMRES = ', num2str(flag),'; # iterations = ', num2str(iter),'; residual = ', num2str(relres),'.'])
disp('-----------------------------------------------------------')

figure(12), semilogy(resvec),     
xlabel('# iterations'), ylabel('residual')
drawnow

%--------------------------------------------------------------------------
%Compute vertical electric field [V/km] and vertical current [A/km^2]
Er   =-Dr*uf;
Jr   = sigma.*Er;
Jr_t = Jr(bdry_top);

%Rt is the total resistance of the domain
%Set all bdrys at zero volts & introduce sources
Rt = 1/-(wghts*Jr_t*rf^2);

%--------------------------------------------------------------------------
uf   = Rt*Is*uf;
Er   =-Dr*uf;
Jr   = sigma.*Er + Jr_s;
Jr_t = Jr(bdry_top);

% total current at top bdry
Itot   = wghts*Jr_t*rf^2;

% positive current at top bdry
gg   = find(Jr_t>0);
Ipos = wghts(gg)*Jr_t(gg)*rf^2;

% potential top bdry
V = Rt*Is;
%--------------------------------------------------------------------------

fprintf('\nIntegrated quantities:\n') ;
disp('-----------------------------------------------------------')
disp(['Global Resistance = ', num2str(Rt),' Ohms.'])
disp(['Source Current at top boundary = ',num2str(Is),' A.'])
disp(['Net current at top boundary = ',num2str(Itot),' A.'])
disp(['Positive current at top boundary = ',num2str(Ipos),' A.'])
disp(['Electric Potential at top boundary = ', num2str(V),' V.'])
disp('-----------------------------------------------------------')


end