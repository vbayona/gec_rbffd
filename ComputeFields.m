function [E,J] = ComputeFields(u,Dlam,Dthe,Dr,sigma)

%--------------------------------------------------------------------------
% ComputeFields - Computes fields E and J from potential u.
%
% INPUT:
%
%   u - electric potential
%   Dlam - Diff. matrix of longitudinal derivative 
%   Dthe - Diff. matrix of latitudinal derivative 
%   Dr - Diff. matrix of radial derivative 
%   sigma - column vector containing the GEC conductivity 
%
% OUTPUT:
%
%   E - electric field
%   J - electric current density field
%
%--------------------------------------------------------------------------

fprintf('\nComputing fields......') ;

% Electric field
Er = -Dr*u;
Ethe = -Dthe*u;
Elam = -Dlam*u;
E=[Er Ethe Elam];

% Current density
Jr = sigma.*Er;
Jthe = sigma.*Ethe;
Jlam = sigma.*Elam;
J=[Jr Jthe Jlam];

fprintf(' done.\n\n');

end