function plotGEC(NodeDistrib,D,X,Y,R0,Y0,int,ext)

%--------------------------------------------------------------------------
% This function makes a [lat,long] plot of variable Y at distance R0 from sea level.
% Requires the outputs of function "InterpMatrix" being stored in the Workspace. 
% The values of Y greater/smaller than the threshold Y0 can be plotted by 
% selecting the threshold Y0. If Y0=[], everything is plotted.
%
% NodeDistrib - name of the file containing the node distribution
% D  - Interpolation matrix
% X  - Cartesian grid on which matrix D interpolates.
% Y  - Variable to plot.
% R0 - Distance [km] from Earth's surface at which the results are plotted
% Y0 - Threshold for variable Y.
%--------------------------------------------------------------------------

%% inputs
%-----------------------------------------------------------------------
load(NodeDistrib,'E','Fb','x','y','z','r0','rf')
%---------------------------
% Evaluation nodes
%---------------------------
xg=X(:,1); yg=X(:,2); zg=X(:,3);
sg = round(size(xg,1)^(1/3));
%-----------------------------------------------------------------------


%% Interpolation of Y at altitude R0
%-----------------------------------------------------------------------
if(~iscolumn(Y)), Y=Y'; end
ui = D*Y;
%---------------------------
th_r=linspace(-pi/2,pi/2,500);
la_r=linspace(-pi,pi,500);
la_r  = repmat(la_r',1,500);
th_r  = repmat(th_r,500,1);
r=Fb(la_r,th_r);
maxR=max(max(r));
la_r=la_r*180/pi+180;
th_r=th_r*180/pi;
%---------------------------
ug = zeros(size(xg));
ug(int) = ui;

% if maxR > E(r0+R0)
%     F=scatteredInterpolant(xg(int),yg(int),zg(int),ui);
%     ug(ext) = F(xg(ext),yg(ext),zg(ext));
% end

ug = reshape(ug,sg,sg,sg);
xg = reshape(xg,sg,sg,sg);
yg = reshape(yg,sg,sg,sg);
zg = reshape(zg,sg,sg,sg);
%---------------------------
e0   = E(r0+R0);
data = sqrt(xg.^2+yg.^2+zg.^2);
%---------------------------
[~,verts,~] = isosurface(xg,yg,zg,data,e0,xg);
[la,th,~]   = cart2sph(verts(:,1),verts(:,2),verts(:,3));
figure(1)
p  = patch(isosurface(xg,yg,zg,data,e0));
nc = isocolors(xg,yg,zg,ug,p);
%---------------------------
tri = delaunay(la,th);
la=la*180/pi+180;
th=th*180/pi;

if(isempty(Y0))

    figure
    h(1)=trisurf(tri,la,th,nc);
    set(h(1),'FaceColor','interp','EdgeColor','none')
    view([0,90]), daspect([1 1 1])
    colorbar, grid off
    [cmin,cmax]=caxis;
    caxis([cmin cmax]);
    grid off
    view([0 90])
    axis equal
    grid off
    colorbar
    ylabel('Latitude','FontSize',16)
    xlabel('Longitude','FontSize',16)
    axis([0 360 -90 90])
    box on
    set(gca,'Xtick',0:90:360,'Ytick',-90:90:90)
    axis([0 360 -90 90])
%     c=colormap(gray);
%     colormap(c(1:54,:))
%     cbr=colorbar;
%     ylabel(cbr,'Potential ({\itk}V)','rotation',-90,'FontSize',16,'Position',[8.8 33.25 1]);
    set(gca,'FontSize',16)

    if maxR >= E(r0+R0)
        r(r<E(r0+R0))=-1e10;
        hold on
        h(2)=surf(la_r,th_r,r-E(r0+R0));
        hold off
        set(h(2),'FaceColor',[0 0 0],'EdgeColor',[1 1 1],'FaceAlpha',1);
    end

else
      
    minz = double(min(nc));
    if minz > Y0
        disp('warning: values lie above threshold selected');
    else
        maxz = double(max(nc));
        if maxz < Y0
            disp('warning: values lie below threshold selected');
        else
                       
            figure, clf
            h(1)=trisurf(tri,la,th,nc);
            set(h(1),'FaceColor','interp','EdgeColor','none')
            view([0,90]), daspect([1 1 1])
            colorbar, grid off
            [~,cmax]=caxis;
            caxis([Y0 cmax]);
            grid off
            view([0 90])
            axis equal
            grid off
            colorbar
            ylabel('Latitude','FontSize',14)
            xlabel('Longitude','FontSize',14)
            axis([0 360 -90 90])
            set(gca,'FontSize',14)
            box on
            set(gca,'Xtick',0:90:360,'Ytick',-90:90:90)
            axis([0 360 -90 90])
            
            newmap = colormap;          
            newmap(1,:) = [1 1 1];      %set there to white
            colormap(newmap);           %activate it

            if maxR >= E(r0+R0)
                r(r<E(r0+R0))=-1e10;
                hold on
                h(2)=surf(la_r,th_r,r-E(r0+R0));
                hold off
                set(h(2),'FaceColor',[0 0 0],'FaceAlpha',1);
            end

            
            figure, clf
            h(1)=trisurf(tri,la,th,nc);
            set(h(1),'FaceColor','interp','EdgeColor','none')
            view([0,90]), daspect([1 1 1])
            colorbar, grid off
            [cmin,~]=caxis;
            caxis([cmin Y0]);
            grid off
            view([0 90])
            axis equal
            grid off
            colorbar
            ylabel('Latitude','FontSize',14)
            xlabel('Longitude','FontSize',14)
            axis([0 360 -90 90])
            set(gca,'FontSize',14)
            box on
            set(gca,'Xtick',0:90:360,'Ytick',-90:90:90)
            axis([0 360 -90 90])
            
            newmap = colormap;          
            newmap(length(newmap),:) = [1 1 1];    %set there to white
            colormap(newmap);                      %activate it   

            if maxR >= E(r0+R0)
                r(r<E(r0+R0))=-1e10;
                hold on
                h(2)=surf(la_r,th_r,r-E(r0+R0));
                hold off
                set(h(2),'FaceColor',[0 0 0],'FaceAlpha',1);
            end                              
        end
    end

end
    
return