GEC-RBFFD model version 0.1
-----------------------------------------------------------------------
This is a numerical solver for the Global Electric Circuit (GEC) based 
on Radial Basis Functions-generated Finite Differences (RBFFD). It can 
be downloaded at https://vbayona@bitbucket.org/vbayona/gec_rbffd.git

As described in the article (doi:10.5194/gmdd-8-3523-2015), we have 
developed two different approaches to solve the GEC. To run each one, 
follow the next steps: 

-----------------------------------------------------------------------
"Dipole approach": compute the GEC solution approximating the TRMM 
data as dipoles.

1. Save the conductivity and source input files in the directories 
"/conductivity" and "/source", respectively. Notice that:
  a) There can be as many files as wanted in these folders. 
  b) The name of the input files can be chosen as desired. 
  c) The name of the input files must be set up in "GEC_settings.m". 
  d) The format and data structure of the input files must be the same 
     as in the example files "out.nc" and "pop_current_1998_2010_1x1.nc". 

2. Set up model parameters in "GEC_settings.m". To calibrate the strength 
of the dipoles:
  a) Select a single dipole (s.dipole = 1) with unit strength (s.N = 1). 
  b) Run "GEC_driver.m".
  c) Compute the current Is at 20km using "IntCurrent_dipole.m".
  d) Select s.dipole = 0 with strength s.N = 1/Is. 

3. Run "GEC_driver.m" to solve the GEC model.

-----------------------------------------------------------------------
"TRMM-BC approach": compute the GEC solution by enforcing the TRMM data 
as a Neumann Boundary Condition at 20km.

1. Same as point (1) from previous section. 

2. Set up model parameters in "GEC_settings.m".

3. Run "GEC_driver.m" to solve the GEC model.

-----------------------------------------------------------------------
To plot the result, we provide two different functions: "plotGEC.m" and 
"plotGEC_layer.m". Please read the help provided in each function.


