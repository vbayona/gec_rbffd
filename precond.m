function u = precond(Lp,f)

persistent L U

[L,U] = ilu(Lp);
u = gmres(Lp,f,20,[],[],L,U);

return