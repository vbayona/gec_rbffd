function [xh, Nh] = NodesType(nodesType,Nh)

if strcmp(nodesType,'md')
    
    aux = floor(sqrt(Nh)-1);
    Nh  = (aux+1)^2;
    
    if Nh<=10
        fname = ['md00',num2str(aux),'.0000',num2str(Nh)];
    elseif Nh<100
        fname = ['md00',num2str(aux),'.000',num2str(Nh)];
    elseif Nh==100
        fname = ['md00',num2str(aux),'.00',num2str(Nh)];
    elseif Nh<1000
        fname = ['md0',num2str(aux),'.00',num2str(Nh)];
    elseif Nh<10000
        fname = ['md0',num2str(aux),'.0',num2str(Nh)];
    elseif Nh==10000
        fname = ['md0',num2str(aux),'.',num2str(Nh)];
    elseif Nh<=27556
        fname = ['md',num2str(aux),'.',num2str(Nh)];
    else
        fname = ['me',num2str(aux),'.0',num2str(Nh)];
    end

elseif strcmp(nodesType,'me')
    
    aux = round(sqrt(Nh)-1);
    Nh  = (aux+1)^2;
   
    if Nh<=10
        fname = ['me00',num2str(aux),'.00000',num2str(Nh)];
    elseif Nh<100
        fname = ['me00',num2str(aux),'.0000',num2str(Nh)];
    elseif Nh==100
        fname = ['me00',num2str(aux),'.000',num2str(Nh)];
    elseif Nh<1000
        fname = ['me0',num2str(aux),'.000',num2str(Nh)];
    elseif Nh<10000
        fname = ['me0',num2str(aux),'.00',num2str(Nh)];
    elseif Nh==10000
        fname = ['me0',num2str(aux),'.0',num2str(Nh)];
    elseif Nh<100000
        fname = ['me',num2str(aux),'.0',num2str(Nh)];
    elseif Nh==100000
        fname = ['me',num2str(aux),'.',num2str(Nh)];
    else
        fname = ['me',num2str(aux),'.',num2str(Nh)];
    end
    
elseif strcmp(nodesType,'ico')
    
    aux = floor(sqrt((Nh-2)/10));
    Nh  = 10*aux^2+2;
    if Nh<100
        fname = ['ico00',num2str(aux),'.0000',num2str(Nh)]; 
    elseif Nh<1000
        fname = ['ico00',num2str(aux),'.000',num2str(Nh)];
    elseif Nh<10000
        fname = ['ico0',num2str(aux),'.00',num2str(Nh)];
    elseif Nh<100000
        fname = ['ico0',num2str(aux),'.0',num2str(Nh)];
    else
        fname = ['ico',num2str(aux),'.',num2str(Nh)];
    end
    
end

xh = load(fname);
Nh = length(xh);

return