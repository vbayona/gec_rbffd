
function nodeDistrib = NodeGenerator(g)

%--------------------------------------------------------------------------
% NodeGenerator - generates node distribution that discretizes the domain.
%
% INPUT: string "g" defined in GEC_settings. If
%
%    g.Topo = 1 - Calls function NodeGenerator_Topo. The Earth's topography
%                 is considered.
%
%    g.Topo = 0 - Calls function NodeGenerator_noTopo. In this case, the 
%                 Earth's topography is neglected and it is assumed to be a sphere.
%
% In both cases, given the parameters from GEC_settings.m, it first looks
% into the "/node_distribution" directory to check if there is a "*.mat" 
% file with these settings. It is loaded if it already exists. Otherwise, it is computed. 
%
% OUTPUT: 
% 
%   nodeDistrib - name of the file containing the node distribution 
%
%--------------------------------------------------------------------------

if g.Topo
    nodeDistrib = NodeGenerator_Topo(g);
else
    nodeDistrib = NodeGenerator_noTopo(g);
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function nodeDistrib = NodeGenerator_Topo(g)

nodeDistrib = strcat(g.nodesType,['_Topo_',num2str(g.Topo),'_horiz_resol_',num2str(g.hg),...
    '_Nr_',num2str(g.Nr),'_rf_',num2str(g.rf),'_beta_',num2str(g.beta),'.mat']);


if ~exist(strcat('node_distribution/',nodeDistrib),'file')
    
    fprintf('\nGenerating node distribution...... ') ;
    
    r0 = g.r0;
    rf = g.rf;
    Nr = g.Nr;
    hg = g.hg;
    Beta = g.beta;
    TopoFile = g.TopoFile;
    nodesType = g.nodesType;
    
    
    %% Change of variables
    %-----------------------------------------------------------------------
    h  = 0.05;
    Nh = floor(4*pi*r0^2/(hg*111)^2);
    e0 = sqrt(Nh/4/pi)*h;
    h  = h/2;
    ef = e0+Nr*h;
    %------------------------------
    K  = (rf-r0)/(exp(Beta*(ef-e0))-1);
    B  = (r0*exp(Beta*(ef-e0))-rf)/(exp(Beta*(ef-e0))-1);
    R  = @(e) K*exp(Beta*(e-e0))+B;
    E  = @(r) e0 + log((r-B)/K)/Beta;
    %-----------------------------------------------------------------------
    
    
    
    %% Load Topography (Bottom Boundary)
    %-----------------------------------------------------------------------
    ncid  = netcdf.open(TopoFile,'NOWRITE');
    %---------------
    varid = netcdf.inqVarID(ncid,'lat');
    Lat   = netcdf.getVar(ncid,varid,'double');
    The   = Lat*pi/180;
    %---------------
    varid = netcdf.inqVarID(ncid,'lon');
    Lon   = netcdf.getVar(ncid,varid,'double');
    Lam   = [(Lon*pi/180-pi); pi];
    %---------------
    varid = netcdf.inqVarID(ncid,'PHIS');
    Z     = netcdf.getVar(ncid,varid,'double');
    r     = Z*1e-4+r0;
    r     = [r; r(1,:)];
    %--------------------------------------------
    % interp. bdry
    %--------------------------------------------
    [lam,the] = ndgrid(Lam,The);
    Fb   = scatteredInterpolant(lam(:),the(:),E(r(:)));
    %--------------------------------------------
    % Fine interp. for bdry generation
    %--------------------------------------------
    nlam = length(Lam); nthe = length(The);
    lam  = repmat(linspace(Lam(1),Lam(nlam),14*nlam)',1,14*nthe);
    the  = repmat(linspace(The(1),The(nthe),14*nthe),14*nlam,1);
    r    = Fb(lam(:),the(:));
    Rmax = max(r);
    Rmin = min(r);
    %-------------
    ind  = find(r>e0+0.1*h);
    r    = r(ind);
    lam  = lam(ind);
    the  = the(ind);
    [~,ind]=sort(-r);
    [xxb,yyb,zzb]=sph2cart(lam(ind),the(ind),r(ind));
    %--------------------------------------------
    clear Lat Lon Z Lam The lam the r
    %-----------------------------------------------------------------------
    
    
    
    
    
    %% Boundary generation
    %-----------------------------------------------------------------------
    [xh, Nh] = NodesType(nodesType,Nh);
    [lam,the] = cart2sph(xh(:,1),xh(:,2),xh(:,3));
    r   = Fb(lam,the);
    ind = find(r<e0+0.1*h);
    %------------
    [xb,yb,zb] = sph2cart(lam(ind),the(ind),r(ind));
    %------------
    xb2=xxb(1);
    yb2=yyb(1);
    zb2=zzb(1);
    for i=2:length(xxb)
        d=min(sqrt((xb2-xxb(i)).^2+(yb2-yyb(i)).^2+(zb2-zzb(i)).^2));
        if (d>0.95*h)
            xb2=[xb2; xxb(i)];
            yb2=[yb2; yyb(i)];
            zb2=[zb2; zzb(i)];
        end
    end
    xb=[xb; xb2];
    yb=[yb; yb2];
    zb=[zb; zb2];
    Nb = length(xb);
    %--------------------------------------------
    [~,~,r]=cart2sph(xb,yb,zb);
    ind_b  = find(r>=e0+0.1*h)';
    %-----------------------------------------------------------------------
    
    
    
    
    %% Ghost nodes generation
    %-----------------------------------------------------------------------
    [lam,the] = cart2sph(xh(:,1),xh(:,2),xh(:,3));
    r=(e0-h)*ones(length(lam),1);
    [xghost,yghost,zghost] = sph2cart(lam,the,r);
    %------------
    [index,d] = knnsearch([xghost yghost zghost],[xb yb zb],'K',3);
    [ind_ghost,~] = unique(index(d<1.1*h));
    %------------
    xghost = xghost(ind_ghost);
    yghost = yghost(ind_ghost);
    zghost = zghost(ind_ghost);
    %-----------------------------------------------------------------------
    
    
    
    
    %% Scattered nodes generation (repul. area)
    %-----------------------------------------------------------------------
    r=[]; lam=[]; the=[];
    %------------------------------
    jmin = floor(abs(e0-Rmin)/h) + 6;
    jmax = floor(abs(e0-Rmax)/h) + 2;
    %------------------------------
    [la,th] = cart2sph(xh(:,1),xh(:,2),xh(:,3));
    for j = -jmin:jmax
        lam = [lam; la(:)];
        the = [the; th(:)];
        r   = [r; (e0+j*h)*ones(Nh,1)];       
    end
    wghts = xh(:,4)';
    %--------------------
    int = find(r>Fb(lam,the));
    ext = find(r<Fb(lam,the));
    [x,y,z] = sph2cart(lam,the,r);
    %-----------------------------------------------------------------------
    
    
    
    
    
    %% int/ext nodes && Repulsion
    %-----------------------------------------------------------------------
    [index,d] = knnsearch([x y z],[xb yb zb],'K',10);
    [ex,~]    = unique(index(d<0.6*h));
    
    [index,d] = knnsearch([x y z],[xb(ind_b) yb(ind_b) zb(ind_b)],'K',1000);
    [in,~]    = unique(index(d<4.0*h));
    [in2,~]   = unique(index(d<6.0*h));
    %--------------------
    int = int(~ismember(int,ex));
    ext = ext(~ismember(ext,ex));
    %--------------------
    indi = intersect(int,in);
    inde = intersect(ext,in);
    %--------------------
    int2 = int(~ismember(int,indi));
    %--------------------
    int  = intersect(int2,in2);
    int2 = setdiff(int2,int);
    %------------------------------------------------------
    xx = [x([ext; int]); xb; x([indi; inde])];
    yy = [y([ext; int]); yb; y([indi; inde])];
    zz = [z([ext; int]); zb; z([indi; inde])];
    %--------------------
    rep_nodes = length([ext; int])+Nb+1:length(xx);
    [xr,yr,zr]=repelf3D(xx,yy,zz,rep_nodes,0,0.75);
    %--------------------
    [index,d] = knnsearch([xr yr zr],[xxb yyb zzb],'K',6);
    [ex,~]    = unique(index(d<0.15*h));
    aux = setdiff(1:length(xr),ex);
    xr=xr(aux);
    yr=yr(aux);
    zr=zr(aux);
    %--------------------
    [la,th,r] = cart2sph(xr,yr,zr);
    indi = find(r>Fb(la,th));
    %-----------------------------------------------------------------------
    
    
    
    
    %% Nodes generation: labeling, sorting, ...
    %-----------------------------------------------------------------------
    xx = [x([int; int2]); xr(indi); xghost; xb]';
    yy = [y([int; int2]); yr(indi); yghost; yb]';
    zz = [z([int; int2]); zr(indi); zghost; zb]';
    %-------------------------------
    N     = length(xx);
    Nint  = length(int)+length(int2);
    Nindi = length(indi);
    Ng    = length(ind_ghost);
    %-------------------------------
    ghost_bot = Nint+Nindi+(1:Ng);
    bdry_bot  = Nint+Nindi+Ng+(1:Nb);
    [~,~,r]   = cart2sph(xx,yy,zz);
    frame     = find(r>max(r)-0.5*h);
    indomain  = setdiff(1:N,[ghost_bot bdry_bot frame]);
    Nd        = length(indomain);
    Nscatter  = Nd+2*Nh;
    %------------------------------------------------------
    % grid nodes generation
    %-----------------------
    [la,th] = cart2sph(xh(:,1),xh(:,2),xh(:,3));
    e       = e0+jmax*h:h:ef+h;
    Nlayer  = length(e);
    
    lam     = repmat(la,1,Nlayer);
    the     = repmat(th,1,Nlayer);
    r       = repmat(e,Nh,1);
    [xlayer,ylayer,zlayer] = sph2cart(lam(:),the(:),r(:));
    
    Nd_max    = length(xlayer)-2*Nh;
    bdry_top  = Nd_max+1:length(xlayer)-Nh;
    ghost_top = Nd_max+Nh+1:length(xlayer);
    %------------------------------------------------------
    % Sort nodes
    %-----------------------
    xdom    = [xx(ghost_bot) xx(indomain)];
    ydom    = [yy(ghost_bot) yy(indomain)];
    zdom    = [zz(ghost_bot) zz(indomain)];
    
    [~,~,r] = cart2sph(xdom,ydom,zdom);
    [~,ind] = sort(r);
    
    ghost_bot = find(ind<=Ng);
    indi      = find((ind>=Ng+1).*(ind<=Ng+Nd));
    Nscatter  = Nscatter+Ng;
    %------------------------------------------------------
    % Nodes & labels
    %-----------------------
    x = [xdom(ind) xlayer' xx(bdry_bot)];
    y = [ydom(ind) ylayer' yy(bdry_bot)];
    z = [zdom(ind) zlayer' zz(bdry_bot)];
    %---------------------------
    N   = length(x);
    Nb  = length(bdry_bot);
    Ng  = length(ghost_bot);
    Nbt = length(bdry_top);
    Ngt = length(ghost_top);
    %---------------------------
    indomain  = [indi length(ind)+(1:Nd_max)];
    Nd        = length(indomain);
    bdry_top  = Ng+Nd+(1:Nbt);
    ghost_top = Ng+Nd+Nbt+(1:Ngt);
    bdry_bot  = Ng+Nd+Nbt+Ngt+(1:Nb);
    %---------------------------
    % smaller stencils
    %-----------------------
    aux=setdiff(1:Nscatter,ghost_bot);
    [index,d] = knnsearch([x(aux)' y(aux)' z(aux)'],[xxb yyb zzb],'K',30);
    [ind_int,~] = unique(index(d<2*h));
    ind_int = aux(ind_int);
    %---------------------------
    
    
    
    %% Save node distribution
    %-----------------------------------------------------------------------
    save(strcat('node_distribution/',nodeDistrib),'h','Beta','rf','r0','e0','ef','wghts','ghost_bot','bdry_bot','indomain',...
        'bdry_top','ghost_top','ind_int','x','y','z','Nh','Nscatter','hg','Nr','K','B','R','E',...
        'Fb','N','Nb','Ng')
       
else
    
    fprintf('\nLoading node distribution...... ') ;  
   
end

fprintf('done.\n') ;

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function nodeDistrib = NodeGenerator_noTopo(g)

nodeDistrib = strcat(g.nodesType,['_Topo_',num2str(g.Topo),'_horiz_resol_',num2str(g.hg),...
    '_Nr_',num2str(g.Nr),'_rf_',num2str(g.rf),'_beta_',num2str(g.beta),'.mat']);


if ~exist(strcat('node_distribution/',nodeDistrib),'file')
    
    fprintf('\nGenerating node distribution...... ') ;
    
    r0 = g.r0;
    rf = g.rf;
    Nr = g.Nr;
    hg = g.hg;
    Beta = g.beta;
    TopoFile = g.TopoFile;
    nodesType = g.nodesType;

    
    %% Change of variables
    %-----------------------------------------------------------------------
    h  = 0.05;
    Nh = floor(4*pi*r0^2/(hg*111)^2);
    e0 = sqrt(Nh/4/pi)*h;
    h  = h/2;    
    ef = e0+Nr*h;
    %------------------------------
    K  = (rf-r0)/(exp(Beta*(ef-e0))-1);
    B  = (r0*exp(Beta*(ef-e0))-rf)/(exp(Beta*(ef-e0))-1);
    R  = @(e) K*exp(Beta*(e-e0))+B;
    E  = @(r) e0 + log((r-B)/K)/Beta;
    %-----------------------------------------------------------------------
    
    
    
    %% Load Topography (Bottom Boundary)
    %-----------------------------------------------------------------------
    ncid  = netcdf.open(TopoFile,'NOWRITE');
    %---------------
    varid = netcdf.inqVarID(ncid,'lat');
    Lat   = netcdf.getVar(ncid,varid,'double');
    The   = Lat*pi/180;
    %---------------
    varid = netcdf.inqVarID(ncid,'lon');
    Lon   = netcdf.getVar(ncid,varid,'double');
    Lam   = [(Lon*pi/180-pi); pi];
    %---------------
    varid = netcdf.inqVarID(ncid,'PHIS');
    Z     = netcdf.getVar(ncid,varid,'double');
    r     = Z*1e-4+r0;
    r     = [r; r(1,:)];
    %--------------------------------------------
    % interp. bdry
    %--------------------------------------------
    [lam,the] = ndgrid(Lam,The);
    r    = E(r(:));
    lam  = lam(:);
    the  = the(:);
    Fb   = scatteredInterpolant(lam,the,r);
    %--------------------------------------------
    % Fine interp. for bdry generation
    %--------------------------------------------
    nlam = length(Lam); nthe=length(The);
    Lam  = linspace(Lam(1),Lam(nlam),14*nlam);
    The  = linspace(The(1),The(nthe),14*nthe);
    lam  = repmat(Lam',1,14*nthe);
    the  = repmat(The,14*nlam,1);
    lam  = lam(:);
    the  = the(:);
    r    = Fb(lam,the);
    Rmax = max(r);
    Rmin = min(r);
    %-------------
    ind  = find(r>e0+0.1*h);
    r    = r(ind);
    lam  = lam(ind);
    the  = the(ind);
    [~,ind]=sort(-r);
    [xxb,yyb,zzb]=sph2cart(lam(ind),the(ind),r(ind));
    %--------------------------------------------
    clear Lat Lon Z Lam The lam the r
    %-----------------------------------------------------------------------
    
    
    
    
    
    %% Boundary generation
    %-----------------------------------------------------------------------
    [xh, Nh] = NodesType(nodesType,Nh);
    [lam,the] = cart2sph(xh(:,1),xh(:,2),xh(:,3));
    [xb,yb,zb] = sph2cart(lam,the,e0*ones(size(lam)));
    Nb = length(xb);
    %--------------------------------------------
    [~,~,r]=cart2sph(xb,yb,zb);
    ind_b  = find(r>=e0+0.1*h)';
    %-----------------------------------------------------------------------
    
    
    
    
    %% Ghost nodes generation
    %-----------------------------------------------------------------------
    [lam,the] = cart2sph(xh(:,1),xh(:,2),xh(:,3));
    r=(e0-h)*ones(length(lam),1);
    [xghost,yghost,zghost] = sph2cart(lam,the,r);
    ind_ghost = 1:length(xghost);
    %-----------------------------------------------------------------------
    
    
    
    
    %% Scattered nodes generation (repul. area)
    %-----------------------------------------------------------------------
    r=[]; lam=[]; the=[];
    %------------------------------
    jmin = floor(abs(e0-Rmin)/h) + 6;
    jmax = floor(abs(e0-Rmax)/h) + 2;
    %------------------------------
    [la,th] = cart2sph(xh(:,1),xh(:,2),xh(:,3));
    for j = -jmin:jmax
        lam = [lam; la(:)];
        the = [the; th(:)];
        r   = [r; (e0+j*h)*ones(Nh,1)];        
    end
    wghts = xh(:,4)';
    %--------------------
    int = find(r>e0);%Fb(lam,the));
    ext = find(r<e0);%Fb(lam,the));
    [x,y,z] = sph2cart(lam,the,r);
    %-----------------------------------------------------------------------
    
    
    
    
    
    %% int/ext nodes && Repulsion
    %-----------------------------------------------------------------------
    [index,d] = knnsearch([x y z],[xb yb zb],'K',10);
    [ex,~]    = unique(index(d<0.6*h));
    
    [index,d] = knnsearch([x y z],[xb(ind_b) yb(ind_b) zb(ind_b)],'K',1000);
    [in,~]    = unique(index(d<4.0*h));
    [in2,~]   = unique(index(d<6.0*h));
    %--------------------
    int = int(~ismember(int,ex));
    ext = ext(~ismember(ext,ex));
    %--------------------
    indi = intersect(int,in);
    inde = intersect(ext,in);
    %--------------------
    int2 = int(~ismember(int,indi));
    %--------------------
    int  = intersect(int2,in2);
    int2 = setdiff(int2,int);
    %------------------------------------------------------
    xx = [x([ext; int]); xb; x([indi; inde])];
    yy = [y([ext; int]); yb; y([indi; inde])];
    zz = [z([ext; int]); zb; z([indi; inde])];
    %--------------------
    C=0.75;
    iter=0;
    rep_nodes = length([ext; int])+Nb+1:length(xx);
    [xr,yr,zr]=repelf3D(xx,yy,zz,rep_nodes,iter,C);
    %--------------------
    [la,th,r] = cart2sph(xr,yr,zr);
    indi = find(r>e0);%Fb(la,th));
    %-----------------------------------------------------------------------
    
    
    
    
    %% Nodes generation: labeling, sorting, ...
    %-----------------------------------------------------------------------
    xx = [x([int; int2]); xr(indi); xghost; xb]';
    yy = [y([int; int2]); yr(indi); yghost; yb]';
    zz = [z([int; int2]); zr(indi); zghost; zb]';
    %-------------------------------
    N     = length(xx);
    Nint  = length(int)+length(int2);
    Nindi = length(indi);
    Ng    = length(ind_ghost);
    %-------------------------------
    ghost_bot = Nint+Nindi+(1:Ng);
    bdry_bot  = Nint+Nindi+Ng+(1:Nb);
    [~,~,r]   = cart2sph(xx,yy,zz);
    frame     = find(r>max(r)-0.5*h);
    indomain  = setdiff(1:N,[ghost_bot bdry_bot frame]);
    Nd        = length(indomain);
    Nscatter  = Nd+2*Nh;
    %------------------------------------------------------
    % grid nodes generation
    %-----------------------
    [la,th] = cart2sph(xh(:,1),xh(:,2),xh(:,3));
    e       = e0+jmax*h:h:ef+h;
    Nlayer  = length(e);
    
    lam     = repmat(la,1,Nlayer);
    the     = repmat(th,1,Nlayer);
    r       = repmat(e,Nh,1);
    [xlayer,ylayer,zlayer] = sph2cart(lam(:),the(:),r(:));
    
    Nd_max    = length(xlayer)-2*Nh;
    bdry_top  = Nd_max+1:length(xlayer)-Nh;
    ghost_top = Nd_max+Nh+1:length(xlayer);
    %------------------------------------------------------
    % Sort nodes
    %-----------------------
    xdom    = [xx(ghost_bot) xx(indomain)];
    ydom    = [yy(ghost_bot) yy(indomain)];
    zdom    = [zz(ghost_bot) zz(indomain)];
    
    [~,~,r] = cart2sph(xdom,ydom,zdom);
    [~,ind] = sort(r);
    
    ghost_bot = find(ind<=Ng);
    indi      = find((ind>=Ng+1).*(ind<=Ng+Nd));
    Nscatter  = Nscatter+Ng;
    %------------------------------------------------------
    % Nodes & labels
    %-----------------------
    x = [xdom(ind) xlayer' xx(bdry_bot)];
    y = [ydom(ind) ylayer' yy(bdry_bot)];
    z = [zdom(ind) zlayer' zz(bdry_bot)];
    %---------------------------
    N   = length(x);
    Nb  = length(bdry_bot);
    Ng  = length(ghost_bot);
    Nbt = length(bdry_top);
    Ngt = length(ghost_top);
    %---------------------------
    indomain  = [indi length(ind)+(1:Nd_max)];
    Nd        = length(indomain);
    bdry_top  = Ng+Nd+(1:Nbt);
    ghost_top = Ng+Nd+Nbt+(1:Ngt);
    bdry_bot  = Ng+Nd+Nbt+Ngt+(1:Nb);
    %---------------------------
    % smaller stencils
    %-----------------------
    ind_int = [];
    %---------------------------
    
    
    
    %% Save node distribution
    %-----------------------------------------------------------------------
    save(strcat('node_distribution/',nodeDistrib),'h','Beta','rf','r0','e0','ef','wghts','ghost_bot','bdry_bot','indomain',...
        'bdry_top','ghost_top','ind_int','x','y','z','Nh','Nscatter','hg','Nr','K','B','R','E',...
        'Fb','N','Nb','Ng')   
    
else
    
    fprintf('\nLoading node distribution...... ') ;

end

fprintf('done.\n') ;

end