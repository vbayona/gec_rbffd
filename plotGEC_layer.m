function plotGEC_layer(NodeDistrib,rplot,u)

%--------------------------------------------------------------------------
% plotGEC_layer makes a [lat,long] plot of variable u at distance rplot from sea level.
% Given  the altitude rplot, it looks for the layer closer to that
% altitude, plotting the solution on this particular layer.
%
% INPUT:
%
%   NodeDistrib - name of the file containing the node distribution
%   rplot - distance from sea level for plotting the results
%   u - variable to plot
%
%--------------------------------------------------------------------------

load(NodeDistrib,'x','y','z','E','R','r0','rf','e0','h','Nr','Fb')

if rplot + r0 > rf 
    disp(['Error: altitude is outside the boundaries of the domain'])
    return
end

%--------------------------------------------------------------------------

[la,th,Er] = cart2sph(x,y,z);
la = la*180/pi+180;
th = th*180/pi;

% compute # layer
layer = round((E(r0 + rplot)-e0)/h);
    e = e0 + h*layer;
  ind = find((Er>e-h/2).*(Er<e+h/2));

%--------------------------------------------------------------------------

la_r = repmat(linspace(-pi,pi,500)',1,500);
th_r = repmat(linspace(-pi/2,pi/2,500),500,1);
   r = Fb(la_r,th_r); 
maxR = max(r(:));
la_r = la_r*180/pi+180;
th_r = th_r*180/pi;

%--------------------------------------------------------------------------

figure(1);
tri = delaunay(la(ind),th(ind));
h(1)=trisurf(tri,la(ind),th(ind),u(ind));
set(h(1),'FaceColor','interp','EdgeColor','none')
view([0,90]), daspect([1 1 1])
colorbar, grid off
[cmin,cmax]=caxis;
caxis([cmin cmax]);
view([0 90])
axis equal
grid off
colorbar
title(['Altitude ',num2str(round(R(e)-r0)),' km'],'FontSize',16)
ylabel('Latitude','FontSize',16)
xlabel('Longitude','FontSize',16)
axis([0 360 -90 90])
box on
set(gca,'Xtick',0:90:360,'Ytick',-90:90:90)
axis([0 360 -90 90])
set(gca,'FontSize',16)

if maxR >= e
    r(r<e)=-1e10;
    hold on
    h(2)=surf(la_r,th_r,r-e);
    hold off
    set(h(2),'FaceColor',[0 0 0],'EdgeColor',[1 1 1],'FaceAlpha',1);
end

%--------------------------------------------------------------------------



