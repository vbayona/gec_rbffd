function [la_s,th_s] = generateSourcePoints(lon,lat,f)

%--------------------------------------------------------------------------
% Generate scattered points from a continous function given in lat-lon.
%
% INPUT:
% 
%   lon - longitude
%   lat - latitude
%   f - number of points in the box [lon,lat]x[lon+d_lon,lat+d_lat]
%
% OUTPUT:
%
%   la_s - longitude of source points
%   th_s - latitude of source points
%
%--------------------------------------------------------------------------

n = round(sum(f(:)));

L_lon = 360;
L_lat = 180;
L_f = max(f(:));

A = L_lat*L_lon;
V = A*L_f;

I = sum(f(:))*A/numel(f);

os = 1.0; % oversampling
X = haltonseq((1:round(os*n*V/I))+randi(1e8,1),3);

X(:,1) = X(:,1)*L_lon-180;
X(:,2) = X(:,2)*L_lat-90;
X(:,3) = X(:,3)*L_f;

s = zeros(round(1.2*n),1);
d_lon = lon(2)-lon(1); d_lat = lat(2)-lat(1);
end_idx = 0;
for i = 1:length(lon)
    for j = 1:length(lat)
        k = find(X(:,1)>=lon(i) & X(:,1)<(lon(i)+d_lon) & X(:,2)>=lat(j) & X(:,2)<(lat(j)+d_lat));
        l = find(X(k,3)<f(i,j));
        s(end_idx+1:end_idx+length(l)) = k(l);
        end_idx = end_idx+length(l);
    end
end
s = s(1:end_idx);
la_s = X(s,1); th_s = X(s,2);

return