function [D,Dp,Dlam,Dthe,Dr]=DiffMatrix(NodeDistrib,lsig_lam,lsig_the,lsig_r,nodesType,save_DiffMatrix)

%--------------------------------------------------------------------------
% DiffMatrix - computes the differentiation matrices for the GEC problem. 
% Given the parameters from GEC_settings.m, this function first looks
% into the "/diff_matrix" directory to check if already exists a "*.mat" 
% file with these settings containing the differentiation matrices. 
% If it does, it is loaded. Otherwise, it is computed. 
%
% INPUT: 
%
%   NodeDistrib - name of the file containing the node distribution
%   lsig_lam - longitudinal derivative of log10(sigma).
%   lsig_the - latitudinal derivative of log10(sigma). 
%   lsig_r - radial derivative of log10(sigma).
%   nodesType - type of nodes used. Defined in g string
%   save_DiffMatrix - flag to save GEC Diff. Matrices
%                     If it is set to 1, they are stored in a "*.mat" file,
%                     in "diff_matrix" directory.
%                     If it is set to 0, they are not stored.
%
% OUTPUT: 
%
%   D - Diff. matrix of GEC problem
%   Dp - Diff matrix of simplified GEC preconditioner problem 
%   Dlam - Diff. matrix of longitudinal derivative 
%   Dthe - Diff. matrix of latitudinal derivative 
%   Dr - Diff. matrix of radial derivative 
% 
%--------------------------------------------------------------------------


diffMatrix = strcat('DM_',NodeDistrib);


if ~exist(strcat('diff_matrix/',diffMatrix),'file')
  
    
    fprintf('\nComputing Diff. Matrices...... ') ;    
    %-----------------------------------------------------------------------
    % inputs
    %-----------------------------------------------------------------------
    load(NodeDistrib)
    %---------------------------
    % MQ-RBF
    %---------------------------
    phi     = @(r2,ep) sqrt(1+ep^2*r2);
    phi_d2  = @(r2,ep) ep.^2./(2.*(ep.^2.*r2 + 1).^(1./2));
    phi_2d2 = @(r2,ep)-ep.^4./(4.*(ep.^2.*r2 + 1).^(3./2)); 
    %---------------------------
    % Variable coeficients
    %---------------------------
    g    = @(e) R(e)/Beta/(R(e)-B);
    f1   = @(h,e2,o2,l2,e1,o1,l1) 2.*h.*(h+e2.*(2-Beta.*h)) + 2.*e1.*(2.*e2+h.*(-2+Beta.*h)).*(cos(l1-l2).*cos(o1).*cos(o2)+sin(o1).*sin(o2));
    f2   = @(h,e2,o2,l2,e1,o1,l1) 4.*(e1.^2.*e2.^2.*cos(o1).^2.*sin(l1-l2).^2 + e1.^2.*e2.^2.*(cos(o2).*sin(o1)-cos(l1-l2).*cos(o1).*sin(o2)).^2 + h.^2.*(-e2+e1.*cos(l1-l2).*cos(o1).*cos(o2)+e1.*sin(o1).*sin(o2)).^2);
    %--------------------------------
    f11 = @(e2,o2,l2,e1,o1,l1) 4.*e1.*e2.*(cos(l1-l2).*cos(o1).*cos(o2)+sin(o1).*sin(o2));
    f22 = @(e2,o2,l2,e1,o1,l1) 4.*e1.^2.*e2.^2.*(cos(o1).^2.*sin(l1-l2).^2+(cos(o2).*sin(o1)-cos(l1-l2).*cos(o1).*sin(o2)).^2);
    %--------------------------------
    f_la = @(e2,o2,l2,e1,o1,l1) -2*e1.*e2.*cos(o1).*sin(l1 - l2);
    f_th = @(e2,o2,l2,e1,o1,l1)  2.*e1.*e2.*(-cos(o2).*sin(o1)+cos(l1-l2).*cos(o1).*sin(o2));
    f_r  = @(e2,o2,l2,e1,o1,l1)  2.*e2-2.*e1.*(cos(l1-l2).*cos(o1).*cos(o2)+sin(o1).*sin(o2));
    %--------------------------------
    % Distance between nodes
    %--------------------------------
    [index,~]=knnsearch([x' y' z'],[x(1:Nscatter)' y(1:Nscatter)' z(1:Nscatter)'],'K',56);
    [index_bdry_bot,~]=knnsearch([x' y' z'],[x(bdry_bot)' y(bdry_bot)' z(bdry_bot)'],'K',56);
    %---------------------------
    % Physical domain
    %---------------------------
    [~,~,Er]=cart2sph(x,y,z);
    %-----------------------------------------------------------------------
    
    
    
    %-----------------------------------------------------------------------
    %  Ghost nodes: bottom bdry (Laplacian)
    %-----------------------------------------------------------------------
    n=56; ep=3;
    %---------------------------
    W  = zeros(n,Ng);
    ind_aux = zeros(n,Ng);
    %---------------------------
    A  = ones(n+1,n+1);
    A(n+1,n+1) = 0;
    b=zeros(n+1,1);
    %---------------------------
    aux = [indomain(1:3*Nh) bdry_bot];
    [ind_dom,~]=knnsearch([x(aux)' y(aux)' z(aux)'],[x(ghost_bot)' y(ghost_bot)' z(ghost_bot)'],'K',n-1);    
    ind_dom  = aux(ind_dom);
    %---------------------------
    s=1;
    for ii=ghost_bot
        %---------------------------
        ind=[ind_dom(ii,:) ii];        
        %---------------------------
        ind_aux(:,s)=ind;
        [lam,the,e]=cart2sph(x(ind)',y(ind)',z(ind)');
        %---------------------------
        [xx,~]=meshgrid(x(ind));
        [yy,~]=meshgrid(y(ind));
        [zz,~]=meshgrid(z(ind));
        r2=(xx-xx').^2+(yy-yy').^2+(zz-zz').^2;
        %---------------------------
        A(1:n,1:n)=phi(r2(1:n,1:n),ep);
        
        b(1:n) = f1(g(e(1)),e(1),the(1),lam(1),e,the,lam)/R(e(1))^2.*phi_d2(r2(1:n,1),ep) + ...
            f2(g(e(1)),e(1),the(1),lam(1),e,the,lam)/R(e(1))^2.*phi_2d2(r2(1:n,1),ep);
        
        w=A\b;
        W(:,s)=w(1:n);
        %---------------------------
        s=s+1;
    end
    I   = repmat(ghost_bot,n,1);
    Lap = sparse(I,ind_aux,W,N,N);
    %-----------------------------------------------------------------------
 
    
    %-----------------------------------------------------------------------
    % Boundary nodes: bottom bdry (gradient)
    %-----------------------------------------------------------------------
    n  = 35; ep = 3;
    %---------------------------
    Wlam = zeros(n,Nb);
    Wthe = zeros(n,Nb);
    Wr   = zeros(n,Nb);
    ind_aux = zeros(n,Nb);
    %--------------------------------
    A=ones(n+1,n+1);
    A(n+1,n+1)=0;
    b=zeros(n+1,3);
    %---------------------------
    s=1;
    for ii=1:Nb;
        %---------------------------
        ind=index_bdry_bot(ii,1:n);
        ind_aux(:,s)=ind;
        %---------------------------
        [xx, xc]=meshgrid(x(ind));
        [yy, yc]=meshgrid(y(ind));
        [zz, zc]=meshgrid(z(ind));
        r2=(xx-xc).^2+(yy-yc).^2+(zz-zc).^2;
        %---------------------------
        [lam,the,e]=cart2sph(x(ind)',y(ind)',z(ind)');
        %---------------------------
        A(1:n,1:n)=phi(r2,ep);
        
        b(1:n,1) = f_la(e(1),the(1),lam(1),e,the,lam).*phi_d2(r2(:,1),ep)/R(e(1));
        b(1:n,2) = f_th(e(1),the(1),lam(1),e,the,lam).*phi_d2(r2(:,1),ep)/R(e(1));
        b(1:n,3) = f_r(e(1),the(1),lam(1),e,the,lam).*phi_d2(r2(:,1),ep)/Beta/(R(e(1))-B);
        %---------------------------
        w = A\b;
        Wlam(:,s) = w(1:n,1);
        Wthe(:,s) = w(1:n,2);
        Wr(:,s)   = w(1:n,3);
        %---------------------------
        s=s+1;
    end
    I    = repmat(bdry_bot,n,1);
    Dlam = sparse(I,ind_aux,Wlam,N,N);
    Dthe = sparse(I,ind_aux,Wthe,N,N);
    Dr   = sparse(I,ind_aux,Wr,N,N);
    %-----------------------------------------------------------------------

        
    %-----------------------------------------------------------------------
    % Scattered region: above topography  (lap & gradient)
    %-----------------------------------------------------------------------
    n  = 56; ep = 3.5;
    aux=setdiff(1:Nscatter,[ghost_bot ind_int]);
    %---------------------------
    W    = zeros(n,length(aux));
    Wlam = zeros(n,length(aux));
    Wthe = zeros(n,length(aux));
    Wr   = zeros(n,length(aux));
    ind_aux = zeros(n,length(aux));
    %--------------------------------
    A=ones(n+1,n+1);
    A(n+1,n+1)=0;
    b=zeros(n+1,4);
    %---------------------------
    s=1;
    for ii=aux;
        %---------------------------
        ind=index(ii,1:n);
        ind_aux(:,s)=ind;
        %---------------------------
        [xx, xc]=meshgrid(x(ind));
        [yy, yc]=meshgrid(y(ind));
        [zz, zc]=meshgrid(z(ind));
        r2=(xx-xc).^2+(yy-yc).^2+(zz-zc).^2;
        %---------------------------
        [lam,the,e]=cart2sph(x(ind)',y(ind)',z(ind)');
        %---------------------------
        A(1:n,1:n)=phi(r2,ep);
        
        b(1:n,1) = f1(g(e(1)),e(1),the(1),lam(1),e,the,lam)/R(e(1))^2.*phi_d2(r2(:,1),ep) +...
            f2(g(e(1)),e(1),the(1),lam(1),e,the,lam)/R(e(1))^2.*phi_2d2(r2(:,1),ep);
        %---------------------------
        b(1:n,2) = f_la(e(1),the(1),lam(1),e,the,lam).*phi_d2(r2(:,1),ep)/R(e(1));
        b(1:n,3) = f_th(e(1),the(1),lam(1),e,the,lam).*phi_d2(r2(:,1),ep)/R(e(1));
        b(1:n,4) = f_r(e(1),the(1),lam(1),e,the,lam).*phi_d2(r2(:,1),ep)/Beta/(R(e(1))-B);
        %---------------------------
        w = A\b;
        W(:,s)    = w(1:n,1);
        Wlam(:,s) = w(1:n,2);
        Wthe(:,s) = w(1:n,3);
        Wr(:,s)   = w(1:n,4);
        %---------------------------
        s=s+1;
    end
    I   = repmat(aux,n,1);
    Lap = Lap + sparse(I,ind_aux,W,N,N);
    %---------------------------------------------------
    Grad_lam = sparse(I,ind_aux,Wlam,N,N);
    Grad_the = sparse(I,ind_aux,Wthe,N,N);
    Grad_r   = sparse(I,ind_aux,Wr,N,N);
    %-----------------------------------------------------------------------
    
    
    %-----------------------------------------------------------------------
    %  Scattered region: over topography (lap & gradient)
    %-----------------------------------------------------------------------
    n   = 56;
    ep  = 3.5;  %laplacian
    ep2 = 3.5;  %gradient
    %---------------------------
    W    = zeros(n,length(ind_int));
    Wlam = zeros(n,length(ind_int));
    Wthe = zeros(n,length(ind_int));
    Wr   = zeros(n,length(ind_int));
    ind_aux = zeros(n,length(ind_int));
    %---------------------------
    A=ones(n+1,n+1);
    A(n+1,n+1)=0;
    b=zeros(n+1,1);
    bh=zeros(n+1,4);
    %---------------------------
    s=1;
    for ii=ind_int;
        %---------------------------
        ind=index(ii,1:n);
        ind_aux(:,s)=ind;
        %---------------------------
        [xx, xc]=meshgrid(x(ind));
        [yy, yc]=meshgrid(y(ind));
        [zz, zc]=meshgrid(z(ind));
        r2=(xx-xc).^2+(yy-yc).^2+(zz-zc).^2;
        %---------------------------
        [lam,the,e]=cart2sph(x(ind)',y(ind)',z(ind)');
        %---------------------------
        A(1:n,1:n)=phi(r2,ep);
        b(1:n) = f1(g(e(1)),e(1),the(1),lam(1),e,the,lam)/R(e(1))^2.*phi_d2(r2(:,1),ep) +...
            f2(g(e(1)),e(1),the(1),lam(1),e,the,lam)/R(e(1))^2.*phi_2d2(r2(:,1),ep);
        
        w = A\b;
        W(:,s)=w(1:n);
        %---------------------------
        A(1:n,1:n)=phi(r2,ep2);
        bh(1:n,1) = f_la(e(1),the(1),lam(1),e,the,lam).*phi_d2(r2(:,1),ep2)/R(e(1));
        bh(1:n,2) = f_th(e(1),the(1),lam(1),e,the,lam).*phi_d2(r2(:,1),ep2)/R(e(1));
        bh(1:n,3) = f_r(e(1),the(1),lam(1),e,the,lam).*phi_d2(r2(:,1),ep2)/Beta/(R(e(1))-B);
        
        w = A\bh;
        Wlam(:,s) = w(1:n,1);
        Wthe(:,s) = w(1:n,2);
        Wr(:,s)   = w(1:n,3);
        %---------------------------
        s=s+1;
    end
    I   = repmat(ind_int,n,1);
    Lap = Lap + sparse(I,ind_aux,W,N,N);
    %---------------------------------------------------
    Grad_lam = Grad_lam + sparse(I,ind_aux,Wlam,N,N);
    Grad_the = Grad_the + sparse(I,ind_aux,Wthe,N,N);
    Grad_r   = Grad_r   + sparse(I,ind_aux,Wr,N,N);
    %---------------------------------------------------
    Dlam = Dlam + Grad_lam;
    Dthe = Dthe + Grad_the;
    Dr   = Dr + Grad_r;
    %-----------------------------------------------------------------------
    
    
    %-----------------------------------------------------------------------
    % Grid Region: Horizontal derivatives (lap & gradient)
    %-----------------------------------------------------------------------
    ng=21; ep=2;
    [xh,~] = NodesType(nodesType,Nh);    
    [la,th] = cart2sph(xh(:,1),xh(:,2),xh(:,3));
    [index_sh,~]=knnsearch([xh(:,1) xh(:,2) xh(:,3)],[xh(:,1) xh(:,2) xh(:,3)],'K',ng);
    %--------------------------------
    ind_hor = zeros(ng,Nh);
    wlap    = zeros(ng,Nh);
    wlam    = zeros(ng,Nh);
    wthe    = zeros(ng,Nh);
    %--------------------------------
    A=ones(ng+1,ng+1);
    A(ng+1,ng+1)=0;
    b=zeros(ng+1,3);
    e=sqrt(Nh/16641)*ones(ng,1);
    %--------------------------------
    s=1;
    for ii=1:Nh
        ind_shell(1:ng) = index_sh(ii,1:ng);
        ind_hor(:,s) = ind_shell;
        %---------------------------
        lam = la(ind_shell);
        the = th(ind_shell);
        %---------------------------
        [xg,yg,zg] = sph2cart(lam,the,e);
        [xx, xc]   = meshgrid(xg);
        [yy, yc]   = meshgrid(yg);
        [zz, zc]   = meshgrid(zg);
        r2 = (xx-xc).^2+(yy-yc).^2+(zz-zc).^2;
        %---------------------------
        A(1:ng,1:ng)=phi(r2,ep);
        
        b(1:ng,1) = f11(e(1),the(1),lam(1),e,the,lam).*phi_d2(r2(:,1),ep) +...
            f22(e(1),the(1),lam(1),e,the,lam).*phi_2d2(r2(:,1),ep);
        b(1:ng,2) = f_la(e(1),the(1),lam(1),e,the,lam).*phi_d2(r2(:,1),ep);
        b(1:ng,3) = f_th(e(1),the(1),lam(1),e,the,lam).*phi_d2(r2(:,1),ep);
        
        w = A\b;
        wlap(:,s) = w(1:ng,1);
        wlam(:,s) = w(1:ng,2);
        wthe(:,s) = w(1:ng,3);
        %---------------------------
        s=s+1;
    end
    
    Nrep  = (N-Nb-Nscatter)/Nh-1;
    %---------------------------
    Wlap  = repmat(wlap,1,Nrep);
    Wlam  = repmat(wlam,1,Nrep);
    Wthe  = repmat(wthe,1,Nrep);
    index = repmat(ind_hor,1,Nrep);
    %---------------------------
    shift_ind = repmat((0:Nrep-1)*Nh,Nh,1);
    shift_ind = repmat(shift_ind(:)',ng,1);
    index     = index + shift_ind + Nscatter;
    %---------------------------
    % Gradient: radial deriv.
    aux    = 1./R(Er(Nscatter+1):h:ef);
    R_grad = repmat(aux,Nh,1);
    R_grad = repmat(R_grad(:)',ng,1);
    Wlam   = Wlam.*R_grad;
    Wthe   = Wthe.*R_grad;
    %---------------------------
    % Laplacian: radial deriv.
    aux   = 1./R(Er(Nscatter+1):h:ef).^2;
    R_lap = repmat(aux,Nh,1);
    R_lap = repmat(R_lap(:)',ng,1);
    Wlap  = Wlap.*R_lap;
    %---------------------------
    I = [repmat(Nscatter+1:N-Nb-2*Nh,ng,1) repmat(ghost_top,ng,1)];
    Grad_lam = Grad_lam + sparse(I,index,Wlam,N,N);
    Grad_the = Grad_the + sparse(I,index,Wthe,N,N);
    Lap = Lap + sparse(I,index,Wlap,N,N);
    %---------------------------
    I = repmat(Nscatter+1:N-Nb-Nh,ng,1);
    Dlam = Dlam + sparse(I,index,Wlam,N,N);
    Dthe = Dthe + sparse(I,index,Wthe,N,N);
    %-----------------------------------------------------------------------
    
    
    
    %-----------------------------------------------------------------------    
    % Radial component: Laplacian
    %-----------------------------------------------------------------------
    aux   = Nscatter+1:N-2*Nh-Nb;
    aux_w = (-1./(R(Er(aux)')-B).^2 + 2./((R(Er(aux)')-B).*R(Er(aux)')))/Beta/h;
    Lap_grid_r = sparse(aux,aux-2*Nh,1/12*aux_w,N,N) + sparse(aux,aux-Nh,   -2/3*aux_w,N,N) + ...
        sparse(aux,aux+Nh,   2/3*aux_w,N,N) + sparse(aux,aux+2*Nh,-1/12*aux_w,N,N);
    
    aux_w = 1./(R(Er(aux)')-B).^2/Beta^2/h^2;
    Lap_grid_2r = sparse(aux,aux-2*Nh,-1/12*aux_w,N,N) + sparse(aux,aux-Nh,  4/3*aux_w,N,N) + ...
        sparse(aux,aux,      -5/2*aux_w,N,N) + sparse(aux,aux+Nh,  4/3*aux_w,N,N) + ...
        sparse(aux,aux+2*Nh,-1/12*aux_w,N,N);
    %-----------------------------------------------------------------------
    %%Bdry top: centered
    aux_w = (-1./(R(Er(bdry_top)')-B).^2 + 2./((R(Er(bdry_top)')-B).*R(Er(bdry_top)')))/Beta/h;
    Lap_r_ghost_top = sparse(ghost_top,bdry_top+Nh, 0.5*aux_w,N,N) + ...
        sparse(ghost_top,bdry_top-Nh,-0.5*aux_w,N,N);
    
    aux_w = 1./(R(Er(bdry_top)')-B).^2/Beta^2/h^2;
    Lap_2r_ghost_top = sparse(ghost_top,bdry_top-Nh,aux_w,N,N) + sparse(ghost_top,bdry_top,-2*aux_w,N,N) +...
        sparse(ghost_top,bdry_top+Nh,aux_w,N,N);
    %-----------------------------------------------------------------------
    
    
    
    %-----------------------------------------------------------------------
    % Radial component: gradient
    %-----------------------------------------------------------------------
    aux   = Nscatter+1:N-2*Nh-Nb;
    aux_w = 1./(R(Er(aux)')-B)/Beta/h;
    Grad_grid_r = sparse(aux,aux-2*Nh,1/12*aux_w,N,N) + sparse(aux,aux-Nh,   -2/3*aux_w,N,N) + ...
        sparse(aux,aux+Nh,   2/3*aux_w,N,N) + sparse(aux,aux+2*Nh,-1/12*aux_w,N,N);
    %-----------------------------------------------------------------------
    %%Bdry top: centered
    aux_w = 1./(R(Er(bdry_top)')-B)/Beta/h;
    Grad_grid_r_ghost_top = sparse(bdry_top+Nh,bdry_top+Nh, 0.5*aux_w,N,N)  + ...
        sparse(bdry_top+Nh,bdry_top-Nh,-0.5*aux_w,N,N);
    %-----------------------------------------------------------------------
    Dr_bdry_top = sparse(bdry_top,bdry_top, 1.5*aux_w,N,N)  + sparse(bdry_top,bdry_top-Nh,-2*aux_w,N,N) + ...
        sparse(bdry_top,bdry_top-2*Nh,0.5*aux_w,N,N);
    %-----------------------------------------------------------------------
      
    
    
    %-----------------------------------------------------------------------
    % Differentiation matrix: Laplacian
    %-----------------------------------------------------------------------
    Lap = Lap + (Lap_grid_2r + Lap_grid_r) + ...
        + (Lap_2r_ghost_top + Lap_r_ghost_top) +...
        sparse(bdry_bot,bdry_bot,ones(Nb,1),N,N) + ...
        sparse(bdry_top,bdry_top,ones(length(bdry_top),1),N,N);
    %-----------------------------------------------------------------------
    % Differentiation matrix: Gradient (radial component)
    %-----------------------------------------------------------------------
    Grad_r = Grad_r + Grad_grid_r + Grad_grid_r_ghost_top;
    %-----------------------------------------------------------------------
    % Differentiation matrix: Dr
    %-----------------------------------------------------------------------
    Dr =  Dr + Grad_grid_r + Dr_bdry_top;
    %-----------------------------------------------------------------------
       
    
    if save_DiffMatrix
        
        % Save diff. matrix
        save(strcat('diff_matrix/',diffMatrix),'Lap','Grad_r','Grad_lam','Grad_the','Dr','Dlam','Dthe')
        
    end  
    
else
    
    fprintf('\nLoading Diff. Matrices...... ') ;
    load(strcat('diff_matrix/',diffMatrix),'Lap','Grad_r','Grad_lam','Grad_the','Dr','Dlam','Dthe')
      
end

%-----------------------------------------------------------------------
% Create Diff. Matrix
%-----------------------------------------------------------------------
N  = length(Lap);
D  = Lap + sparse(1:N,1:N,lsig_r,N,N)*Grad_r +...
     sparse(1:N,1:N,lsig_the,N,N)*Grad_the + ...
     sparse(1:N,1:N,lsig_lam,N,N)*Grad_lam;
Dp = Lap + sparse(1:N,1:N,1/6*ones(N,1),N,N)*Grad_r;
%-----------------------------------------------------------------------
fprintf('done.\n') ;


end