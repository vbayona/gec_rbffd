function f = evaluateSource(la,th,r,xs0,a,b)
%%% f = evaluateSource(xh,r,xs0,a,b)
% Evaluate Gaussian source in spherical coordinates

r0 = xs0(3);
d_sph = @(xs,xs0) r0*atan2(sqrt((cos(xs(:,2)).*sin(xs(:,1)-xs0(1))).^2+ ...
    (cos(xs0(2))*sin(xs(:,2))-sin(xs0(2))*cos(xs(:,2)).*cos(xs(:,1)-xs0(1))).^2),...
    sin(xs0(2))*sin(xs(:,2))+cos(xs0(2))*cos(xs(:,2)).*cos(xs(:,1)-xs0(1)));

f = exp(-d_sph([la th],xs0(1:2)).^2/b^2).*exp(-(r-xs0(3)).^2/a^2)/(a*b^2*pi^(3/2));