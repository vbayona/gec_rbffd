function [sigma,log_sig_lam,log_sig_the,log_sig_r,F_sigma] = conductivity(NodeDistrib,nodesType,c,s,save_DiffMatrix)

%--------------------------------------------------------------------------
% Conductivity - computes the conductivity. 
%
% INPUT: 
%
%   NodeDistrib - name of the file containing the node distribution
%   nodesType - type of nodes used (defined in "g" string)
%   c - string defining conductivity. 
%   s - string defining source term.
%   save_DiffMatrix - flag to save Diff. Matrices. of conductivity
%                     If it is set to 1, they are stored in a "*.mat" file,
%                     in "diff_matrix" directory.
%                     If it is set to 0, they are not stored.
%
% OUTPUT: 
%
%   sigma - conductivity.
%   log_sig_lam - longitudinal derivative of log10(sigma).
%   log_sig_the - latitudinal derivative of log10(sigma). 
%   log_sig_r - radial derivative of log10(sigma).
%   F_sigma - conductivity interpolant from given data to "NodeDistrib".
%
%--------------------------------------------------------------------------



if c.analyticCond
    
    
    fprintf('\nComputing conductivity...... ') ;
    
    %% Inputs
    %-----------------------------------------------------------------------
    load(strcat('node_distribution/',NodeDistrib),'x','y','z','R','bdry_top','ghost_top','r0')
    %---------------------------
    
    
    %% Gradient of conductivity
    %-----------------------------------------------------------------------
    [~,~,r]=cart2sph(x,y,z);
    
    sigma=c.s0*exp((R(r')-r0)/c.h);
    log_sig_lam = zeros(size(x))';
    log_sig_the = zeros(size(x))';
    log_sig_r   = 1/c.h*ones(size(x))';
    
    log_sig_lam(ghost_top) = log_sig_lam(bdry_top);
    log_sig_the(ghost_top) = log_sig_the(bdry_top);
    log_sig_r(ghost_top)   = log_sig_r(bdry_top);
    %-----------------------------------------------------------------------
    F_sigma = @(r) c.s0*exp((R(r')-r0)/c.h);
    %-----------------------------------------------------------------------
    
    fprintf('done.\n') ;
    
    
else
    
    
    fprintf('\nComputing conductivity...... ') ;
    
    %% Inputs
    %-----------------------------------------------------------------------
    load(strcat('node_distribution/',NodeDistrib))
    
    
    %% load conductivity
    %-----------------------------------------------------------------------
    ncid = netcdf.open(c.CondFile,'NOWRITE');
    %-------------
    varid = netcdf.inqVarID(ncid,'lat');
    Lat   = netcdf.getVar(ncid,varid,'double');
    The   = Lat*pi/180;
    %-------------
    varid = netcdf.inqVarID(ncid,'lon');
    Lon   = netcdf.getVar(ncid,varid,'double');
    Lam   = [(Lon*pi/180-pi); pi];
    %-------------
    varid = netcdf.inqVarID(ncid,'Z3');
    Z = netcdf.getVar(ncid,varid,'double');
    Z = Z*1e-3 + r0; % convert to km (it was in m)
    Z = [Z; Z(1,:,:)];
    %-------------
    varid = netcdf.inqVarID(ncid,c.clouds);
    sigma = netcdf.getVar(ncid,varid,'double');
    %Sig = sigma(:,:,:,s.month); % Only keep the time we're interested in    
    Sig = sigma(:,:,:,1); % Only keep the time we're interested in
    Sig = Sig*1e3; % convert to S/km (it was in S/m)
    Sig = [Sig; Sig(1,:,:)];
    
%     F_sigma = @(r) c.s0*exp((R(r)-r0)/c.h);
%     Sig = F_sigma(E(Z));
    %-------------
    [lam,the] = ndgrid(Lam,The);
    lam = repmat(lam,[1 1 size(Sig,3)]);
    the = repmat(the,[1 1 size(Sig,3)]);
    %-----------------------------------------------------------------------
    resol = min(360/length(Lon),180/length(Lat))/1.3;
    %-----------------------------------------------------------------------
    
    
    
    
%     %% load conductivity
%     %-----------------------------------------------------------------------
%     ncid = netcdf.open(c.CondFile,'NOWRITE');
%     %-------------
%     varid = netcdf.inqVarID(ncid,'lat');
%     Lat   = netcdf.getVar(ncid,varid,'double');
%     The   = Lat*pi/180;
%     %-------------
%     varid = netcdf.inqVarID(ncid,'lon');
%     Lon   = netcdf.getVar(ncid,varid,'double');
%     Lam   = [(Lon*pi/180-pi); pi];
%     %-------------
%     varid = netcdf.inqVarID(ncid,'lev');
%     lev = netcdf.getVar(ncid,varid,'double');
%     lev = lev*1e-3 + r0; % convert to km (it was in m)
%     %Z = [Z; Z(1,:,:)];
%     %-------------
%     varid = netcdf.inqVarID(ncid,c.clouds);
%     sigma = netcdf.getVar(ncid,varid,'double');
%     Sig = sigma(:,:,:,s.month); % Only keep the time we're interested in
%     Sig = Sig*1e3; % convert to S/km (it was in S/m)
%     Sig = [Sig; Sig(1,:,:)];
%     Sig(Sig(:)>1) = 1e-10;
%     %-------------
%     [lam,the] = ndgrid(Lam,The);
%     lam = repmat(lam,[1 1 size(Sig,3)]);
%     the = repmat(the,[1 1 size(Sig,3)]);
%     
%     Z = zeros(size(lam));
%     for j=1:length(lev)
%         Z(:,:,j) = repmat(lev(j),size(lam,1),size(lam,2));
%     end
%     %-----------------------------------------------------------------------
%     resol = min(360/length(Lon),180/length(Lat))/1.3;
%     %-----------------------------------------------------------------------
    
    
    
    
    
    %% Node interpolation
    %-----------------------------------------------------------------------
    % collocation nodes
    %-----------------------------------------------------------------------
    ind   = find(Z(:) < rf+5);
    lam_c = lam(ind);
    the_c = the(ind);
    xi_c  = E(Z(ind));
    Sig   = Sig(ind);
    [x_c,y_c,z_c] = sph2cart(lam_c,the_c,xi_c);
    F_sigma       = scatteredInterpolant(x_c,y_c,z_c,Sig);%,'nearest');
    %-----------------------------------------------------------------------
    % evaluation nodes
    %-----------------------------------------------------------------------
    Nh  = floor(4*pi*r0^2/(resol*111)^2);
    [xh,Nh] = NodesType(nodesType,Nh);
    [la,th] = cart2sph(xh(:,1),xh(:,2),xh(:,3));
    e       = e0:h:ef+h;
    Nrep    = length(e);
    lam     = repmat(la,1,Nrep);
    the     = repmat(th,1,Nrep);
    Er      = repmat(e,Nh,1);
    %-------------
    lam = lam(:);
    the = the(:);
    Er  = Er(:);
    [xlayer,ylayer,zlayer] = sph2cart(lam,the,Er);
    N = length(xlayer);
    %-------------
    sigma = F_sigma(xlayer,ylayer,zlayer);
%     F_sigma_interp = scatteredInterpolant(xlayer,ylayer,zlayer,sigma);
    %-----------------------------------------------------------------------
      
    
    diffMatrix = strcat('DM_conductivity_',num2str(resol),'_',nodesType,'_Nr_',num2str(Nr),'.mat');
    
    if ~exist(strcat('diff_matrix/',diffMatrix),'file')
        
        
        %---------------------------
        % GA-RBF function
        %---------------------------
        phi = @(r2,ep) exp(-ep^2*r2);
        phi_d2 = @(r2,ep) -ep.^2*exp(-ep^2*r2);
        %--------------------------------
        % Deriv. change of var.
        %---------------------------
        f_la = @(e2,o2,l2,e1,o1,l1) -2*e1.*e2.*cos(o1).*sin(l1 - l2);
        f_th = @(e2,o2,l2,e1,o1,l1)  2.*e1.*e2.*(-cos(o2).*sin(o1)+cos(l1-l2).*cos(o1).*sin(o2));
        %-----------------------------------------------------------------------
                    
        
        %% Diff. matrix: radial component Gradient
        %-----------------------------------------------------------------------
        aux  = Nh+1:N-Nh;
        aux_w = (0.5/h/Beta)./(R(Er(aux)')-B);
        Dr = sparse(aux,aux-Nh,-aux_w,N,N) + sparse(aux,aux+Nh,aux_w,N,N);
        %-----------------------------------------------------------------------
               
        
        %% Diff. matrix: horiz. component Gradient
        %-----------------------------------------------------------------------
        ng=21; ep=2;
        [index_sh,~]=knnsearch([xh(:,1) xh(:,2) xh(:,3)],[xh(:,1) xh(:,2) xh(:,3)],'K',ng);
        %--------------------------------
        ind_aux = zeros(ng,Nh);
        wlam    = zeros(ng,Nh);
        wthe    = zeros(ng,Nh);
        %--------------------------------
        A=ones(ng+1,ng+1);
        A(ng+1,ng+1)=0;
        b=zeros(ng+1,3);
        er=sqrt(Nh/16641)*ones(ng,1);
        %--------------------------------
        s=1;
        for ii=1:Nh
            ind_shell(1:ng) = index_sh(ii,1:ng);
            ind_aux(:,s) = ind_shell;
            %---------------------------
            lam = la(ind_shell);
            the = th(ind_shell);
            %---------------------------
            [xg,yg,zg] = sph2cart(lam,the,er);
            [xx, xc]   = meshgrid(xg);
            [yy, yc]   = meshgrid(yg);
            [zz, zc]   = meshgrid(zg);
            r2 = (xx-xc).^2+(yy-yc).^2+(zz-zc).^2;
            %---------------------------
            A(1:ng,1:ng)=phi(r2,ep);
            
            b(1:ng,1) = f_la(er(1),the(1),lam(1),er,the,lam).*phi_d2(r2(:,1),ep);
            b(1:ng,2) = f_th(er(1),the(1),lam(1),er,the,lam).*phi_d2(r2(:,1),ep);
            
            w = A\b;
            wlam(:,s) = w(1:ng,1);
            wthe(:,s) = w(1:ng,2);
            %---------------------------
            s=s+1;
        end
        %---------------------------
        Wlam  = repmat(wlam,1,Nrep-2);
        Wthe  = repmat(wthe,1,Nrep-2);
        index = repmat(ind_aux,1,Nrep-2);
        %---------------------------
        shift_ind = repmat((1:Nrep-2)*Nh,Nh,1);
        shift_ind = repmat(shift_ind(:)',ng,1);
        index     = index + shift_ind;
        %---------------------------
        aux    = 1./R(e(2:Nrep-1));
        R_grad = repmat(aux,Nh,1);
        R_grad = repmat(R_grad(:)',ng,1);
        Wlam   = Wlam.*R_grad;
        Wthe   = Wthe.*R_grad;
        %---------------------------
        I = repmat(Nh+1:N-Nh,ng,1);
        Dlam = sparse(I,index,Wlam,N,N);
        Dthe = sparse(I,index,Wthe,N,N);
        %-----------------------------------------------------------------------
        
        if save_DiffMatrix
            
            % Save diff. matrix
            save(strcat('diff_matrix/',diffMatrix),'Dr','Dlam','Dthe')
            
        end
        
    else
        
        load(strcat('diff_matrix/',diffMatrix),'Dr','Dlam','Dthe')
       
    end
    
    
    %% Gradient of conductivity
    %-----------------------------------------------------------------------
    log_sig_lam = Dlam*log(sigma);
    log_sig_the = Dthe*log(sigma);
    log_sig_r   = Dr*log(sigma);
    
    clear Dr Dthe Dlam
    
    [log_sig_lam,log_sig_the,log_sig_r] = PieceInterp(e0,h,Nrep,xlayer,ylayer,zlayer,x,y,z,log_sig_lam,log_sig_the,log_sig_r);
    
%     F_log_sig_lam = scatteredInterpolant(xlayer,ylayer,zlayer,log_sig_lam);
%     F_log_sig_the = scatteredInterpolant(xlayer,ylayer,zlayer,log_sig_the);
%     F_log_sig_r   = scatteredInterpolant(xlayer,ylayer,zlayer,log_sig_r);
    
%     sigma = F_sigma_interp(x,y,z)';
    sigma = F_sigma(x,y,z)';
%     log_sig_lam = F_log_sig_lam(x,y,z);
%     log_sig_the = F_log_sig_the(x,y,z);
%     log_sig_r   = F_log_sig_r(x,y,z);
    
    log_sig_lam(ghost_top) = log_sig_lam(bdry_top);
    log_sig_the(ghost_top) = log_sig_the(bdry_top);
    log_sig_r(ghost_top)   = log_sig_r(bdry_top);
    %-----------------------------------------------------------------------
     
    fprintf('done.\n') ;
    
end

%save('conductivity/data_cond','F_sigma','F_log_sig_lam','F_log_sig_the','F_log_sig_r')

return