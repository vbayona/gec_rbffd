function source = Source(NodeDistrib,s,topo)

[la_s,th_s] = SourceDistribution(s);

fprintf('\nComputing source term...... ') ;

%% Inputs
%-----------------------------------------------------------------------
load(strcat('node_distribution/',NodeDistrib),'x','y','z','R','r0')


%% Compute right hand side from source distribution
%-----------------------------------------------------------------------
% evaluation nodes
[la,th,Er] = cart2sph(x',y',z');
r   = R(Er);

% to speed up the computations, we only consider the nodes inside a window 
% for evaluating the rhs. Tis is possible for the fast decaying of the
% Gaussian.
ind = find((r>r0+s.zn-4.5).*(r<r0+s.zp+4.5));
% ind = find(r<r0+s.zp+20);
r   = r(ind);
la  = la(ind);
th  = th(ind);

if topo
    load(NodeDistrib,'Fb')
    r_s=R(Fb(la_s,th_s));
else
    r_s=r0*ones(size(la_s));
end

% source evaluation
f = zeros(length(ind),1);
for i=1:size(r_s,1)
    f = f + ...
        s.Qp/s.tau*evaluateSource(la,th,r,[la_s(i) th_s(i) r_s(i)+s.zp],s.a,s.b) + ...
        s.Qn/s.tau*evaluateSource(la,th,r,[la_s(i) th_s(i) r_s(i)+s.zn],s.a,s.b);
end
%-----------------------------------------------------------------------

source = zeros(length(x),1);
source(ind) = f;

fprintf('done.\n') ;

return