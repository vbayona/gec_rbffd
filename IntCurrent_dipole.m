function Is = IntCurrent_dipole(NodeDistrib,rs,u,Dr,sigma)

%--------------------------------------------------------------------------
% This function computes the net current on a spherical surface at altitude
% rs f.s.l. It is needed to calibrate strength of dipoles (dipole approach).
%
% How to use it:
% First, run GEC_driver with single dipole (s.dipole=1) and strength S.N=1
% Then, use this function to compute the net current Is at rs=20km f.s.l.
% Finally, set up S.N=1/Is & s.dipole=0. Run GEC_driver.
%
% INPUT:
%   
%   NodeDistrib - name of the file containing the node distribution
%   rs - distance from sea level to integrate the current
%   u - electric potential
%   Dr - diff. matrix to compute radial derivative
%   sigma - conductivity on NodeDistrib
%
% OUTPUT
%
%   Is - net current on spherical surface at altitude rs from sea level
%
%--------------------------------------------------------------------------

load(NodeDistrib,'x','y','z','E','r0','rf','e0','h','wghts')

if rs + r0 > rf 
    disp(['Error: altitude is outside the boundaries of the domain'])
    return
end

%--------------------------------------------------------------------------

[~,~,r] = cart2sph(x,y,z);

% compute # layer
layer = round((E(r0 + rs)-e0)/h);
    e = e0 + h*layer;
  ind = find((r>e-h/2).*(r<e+h/2));
  
Er    = -Dr*u(:,3);
Jr    = sigma.*Er;
Jr_t  = Jr(ind);
Is    = wghts*Jr_t*(r0+rs)^2;

%--------------------------------------------------------------------------

end
