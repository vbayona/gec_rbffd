function [g,c,s] = GEC_settings

%--------------------------------------------------------------------------
% GEC_settings: set up the parameters for the GEC solver. 
%               There are three different fields:
%--------------------------------------------------------------------------
% g - GEOMETRY: define domain size, topography, stretching parameter & node distribution.
%
% c - CONDUCTIVITY: define conductivity. It can be a simple exponential analytical 
%                function (c.analyticCond = 1) or an input from a data file (c.analyticCond = 0).
%
% s - SOURCE: define source data file, dipole parameters, etc... 
%--------------------------------------------------------------------------


addpath('topography',genpath('node_distribution'),'conductivity','source','diff_matrix')


%--------------------------------------------------------------------------
% GEOMETRY
%--------------------------------------------------------------------------
g.r0   = 6371;        % [km] Earth's radius
g.rf   = g.r0 + 60;   % [km] Top boundary
g.Nr   = 120;         % # nodes in radial direction
g.hg   = 1.5;           % [degrees] horizontal resolution
g.beta = 0.05;        % stretching constant in radial coordinate
g.nodesType = 'ico';  % types of nodes: 'ico' or 'md' (suggested 'ico') 
%--------------------------------------------------------------------------
g.Topo = 1;           % if 1 consider Earth's topography
                      % if 0 neglect Earth's topography. 
%--------------------------------------------------------------------------
% g.TopoFile - Name of the NetCDF file containing the Earth's topography. 
%              It must be located inside "/nodes_distribution" directory
%--------------------------------------------------------------------------
g.TopoFile = 'USGS-gtopo30_1.9x2.5_remap_c050602.nc';  
%--------------------------------------------------------------------------



%--------------------------------------------------------------------------
% CONDUCTIVITY
%--------------------------------------------------------------------------
c.analyticCond = 0;     % if 1 consider analytical conductivity
                        % if 0 consider NetCDF file
%--------------------------------------------------------------------------
% NetCDF file: c.analyticCond = 0
%--------------------------------------------------------------------------
% c.CondFile - Name of the NetCDF file containing the conductivity. 
%              It must be located inside "/conductivity" directory
% c.clouds   - parameter within the NetCDF file
%--------------------------------------------------------------------------
c.CondFile = 'out.nc'; 
c.clouds   = 'con_nocl';
%--------------------------------------------------------------------------
% Simple exponential analytical function: c.analyticCond = 1 
% s = c.so*exp(r/c.h)
%--------------------------------------------------------------------------
c.s0 = 5e-11;           % [S/km]
c.h  = 6;               % [km]
%--------------------------------------------------------------------------



%--------------------------------------------------------------------------
% SOURCE
%--------------------------------------------------------------------------
% s.SourceFile - Name of the NetCDF file containing the source distribution. 
%               It must be located inside "/source" directory
%--------------------------------------------------------------------------
s.SourceFile = 'pop_current_1998_2010_1x1.nc';
%--------------------------------------------------------------------------
s.coarsen_box = 2;  % combine 0/2/4 boxes in each direction for source generation
s.N      = 1/0.244667235765086;     % [.]  (constant) 
s.Qn     =-60*s.N;  % [C]  (strength)
s.Qp     = 60*s.N;  % [C]  (strength)
s.tau    = 60;      % [s]  (time scale)
s.a      = 1.5;     % [km] (vertical extent)
s.b      = 300;     % [km] (horizontal extent)
s.zn     = 7;       % [km] (lower center)
s.zp     = 15;      % [km] (upper center)
s.month  = 4;       % month
s.UT     = 12;      % UT time
%------------------------------------------
s.dipole = 0;       % set to 1 for a single dipole located at:
s.la_0   = 180;     % dipole: longitude coordinate [degrees]
s.th_0   = 0;       % dipole: latitude coordinate  [degrees]
%--------------------------------------------------------------------------


end