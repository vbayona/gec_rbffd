function [u,Jr_s,I] = BC_current(NodeDistrib,s,F_sigma,nodesType,analyticCond)

%--------------------------------------------------------------------------
% BC_current - solve the source equation of the GEC problem by enforcing 
% the TRMM data as a Neumann Boundary Condition at 20km. 
%
% INPUT:
%
%   NodeDistrib - name of the file containing the node distribution
%   s - string defining source term.
%   F_sigma - conductivity interpolant from given data to "NodeDistrib".
%   nodesType - type of nodes used (defined in "g" string)
%   analyticCond - flag to set up analytical or numerical conductivity
%
% OUTPUT:
%
%   u - electric potential (V) due to sources 
%   Jr_s - radial current density due to sources (A/m^2)
%   Is - current (A) at top boundary due to sources
%
%--------------------------------------------------------------------------




fprintf('\nSource potential...... ') ;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pre-processing: Grid, conductivity, source term, diff. matrix, etc...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------
% Inputs
%-----------------------------------------------------------------------
load(strcat('node_distribution/',NodeDistrib),'r0','rf','R','E','Nh','B','Beta')
%---------------------------
rb = r0+20;
e0 = E(rb);
ef = E(rf);
Ne = 123;
[xh,Nh] = NodesType(nodesType,Nh);
[la,th] = cart2sph(xh(:,1),xh(:,2),xh(:,3));
%-------------
e   = linspace(e0,ef,Ne-2);
h   = e(2)-e(1);
e   = [e0-h e ef+h];
lam = repmat(la,1,Ne);
the = repmat(th,1,Ne);
Er  = repmat(e,Nh,1);
%-------------
N   = Nh*Ne;
[x,y,z] = sph2cart(lam(:),the(:),Er(:));
%-------------
ghost_bot = 1:Nh;
bdry_bot  = Nh+1:2*Nh;
bdry_top  = N-2*Nh+1:N-Nh;
ghost_top = N-Nh+1:N;
%-----------------------------------------------------------------------


%-----------------------------------------------------------------------
% conductivity
%-----------------------------------------------------------------------
if analyticCond
    [~,~,r]=cart2sph(x',y',z');    
    sigma = F_sigma(r);
else
    sigma = F_sigma(x,y,z);
end
%-----------------------------------------------------------------------


%-----------------------------------------------------------------------
% Source Data
%-----------------------------------------------------------------------
ncid = netcdf.open(s.SourceFile,'NOWRITE');
%-------------
varid = netcdf.inqVarID(ncid,'current');
current = netcdf.getVar(ncid,varid,'double');

varid = netcdf.inqVarID(ncid,'lat');
lat = netcdf.getVar(ncid,varid,'double');

varid = netcdf.inqVarID(ncid,'lon');
lon = netcdf.getVar(ncid,varid,'double');

lon = lon*pi/180-pi;
lat = lat*pi/180;

nlat = length(lat);
nlon = length(lon);

lon  = repmat(lon,1,nlat);
lat  = repmat(lat',nlon,1);
fs = current(:,:,s.UT,s.month)/111^2; % 1 degree = 111km
Fs = scatteredInterpolant(lon(:),lat(:),fs(:),'nearest');

source = Fs(la,th);
%-----------------------------------------------------------------------




%-----------------------------------------------------------------------
% Grid Region: Horizontal derivatives
%-----------------------------------------------------------------------
ng=21; ep=2;
[index_sh,~]=knnsearch([xh(:,1) xh(:,2) xh(:,3)],[xh(:,1) xh(:,2) xh(:,3)],'K',ng);
%--------------------------------
phi     = @(r2,ep) sqrt(1+ep^2*r2);
phi_d2  = @(r2,ep) ep.^2./(2.*(ep.^2.*r2 + 1).^(1./2));
phi_2d2 = @(r2,ep)-ep.^4./(4.*(ep.^2.*r2 + 1).^(3./2));
%--------------------------------
f11 = @(e2,o2,l2,e1,o1,l1) 4.*e1.*e2.*(cos(l1-l2).*cos(o1).*cos(o2)+sin(o1).*sin(o2));
f22 = @(e2,o2,l2,e1,o1,l1) 4.*e1.^2.*e2.^2.*(cos(o1).^2.*sin(l1-l2).^2+(cos(o2).*sin(o1)-cos(l1-l2).*cos(o1).*sin(o2)).^2);
%--------------------------------
f_la = @(e2,o2,l2,e1,o1,l1) -2*e1.*e2.*cos(o1).*sin(l1 - l2);
f_th = @(e2,o2,l2,e1,o1,l1)  2.*e1.*e2.*(-cos(o2).*sin(o1)+cos(l1-l2).*cos(o1).*sin(o2));
%--------------------------------
ind_hor = zeros(ng,Nh);
wlap    = zeros(ng,Nh);
wlam    = zeros(ng,Nh);
wthe    = zeros(ng,Nh);
%--------------------------------
A=ones(ng+1,ng+1);
A(ng+1,ng+1)=0;
b=zeros(ng+1,3);
er=sqrt(Nh/16641)*ones(ng,1);
%--------------------------------
for ii=1:Nh
    ind_shell = index_sh(ii,1:ng);
    ind_hor(:,ii) = ind_shell;
    %---------------------------
    lam = la(ind_shell);
    the = th(ind_shell);
    %---------------------------
    [xg,yg,zg] = sph2cart(lam,the,er);
    [xx, xc]   = meshgrid(xg);
    [yy, yc]   = meshgrid(yg);
    [zz, zc]   = meshgrid(zg);
    r2 = (xx-xc).^2+(yy-yc).^2+(zz-zc).^2;
    %---------------------------
    A(1:ng,1:ng)=phi(r2,ep);
    
    b(1:ng,1) = f11(er(1),the(1),lam(1),er,the,lam).*phi_d2(r2(:,1),ep) +...
        f22(er(1),the(1),lam(1),er,the,lam).*phi_2d2(r2(:,1),ep);
    b(1:ng,2) = f_la(er(1),the(1),lam(1),er,the,lam).*phi_d2(r2(:,1),ep);
    b(1:ng,3) = f_th(er(1),the(1),lam(1),er,the,lam).*phi_d2(r2(:,1),ep);
    
    w = A\b;
    wlap(:,ii) = w(1:ng,1);
    wlam(:,ii) = w(1:ng,2);
    wthe(:,ii) = w(1:ng,3);
end
%---------------------------
Wlap  = repmat(wlap,1,Ne-2);
Wlam  = repmat(wlam,1,Ne-2);
Wthe  = repmat(wthe,1,Ne-2);
index = repmat(ind_hor,1,Ne-2);
%---------------------------
shift_ind = repmat((0:Ne-3)*Nh,Nh,1);
shift_ind = repmat(shift_ind(:)',ng,1);
index     = index + shift_ind + Nh;
%---------------------------
% Gradient: radial deriv.
aux    = 1./R(e(2:Ne-1));
R_grad = repmat(aux,Nh,1);
R_grad = repmat(R_grad(:)',ng,1);
Wlam   = Wlam.*R_grad;
Wthe   = Wthe.*R_grad;
%---------------------------
% Laplacian: radial deriv.
aux   = 1./R(e(2:Ne-1)).^2;
R_lap = repmat(aux,Nh,1);
R_lap = repmat(R_lap(:)',ng,1);
Wlap  = Wlap.*R_lap;
%---------------------------
aux = 2*Nh+1:N-2*Nh;
I   = [repmat(ghost_bot,ng,1) repmat(aux,ng,1) repmat(ghost_top,ng,1)];
Grad_lam = sparse(I,index,Wlam,N,N);
Grad_the = sparse(I,index,Wthe,N,N);
Lap = sparse(I,index,Wlap,N,N);
%---------------------------
I   = repmat(Nh+1:N-Nh,ng,1);
Dlam = sparse(I,index,Wlam,N,N);
Dthe = sparse(I,index,Wthe,N,N);
%-----------------------------------------------------------------------




%-----------------------------------------------------------------------
% Radial component: Laplacian
%aux = 2*Nh+1:N-2*Nh;
aux_w = (-1./(R(Er(aux)')-B).^2 + 2./((R(Er(aux)')-B).*R(Er(aux)')))/Beta/h;
Lap_r = sparse(aux,aux-2*Nh,1/12*aux_w,N,N) + sparse(aux,aux-Nh,   -2/3*aux_w,N,N) + ...
    sparse(aux,aux+Nh,   2/3*aux_w,N,N) + sparse(aux,aux+2*Nh,-1/12*aux_w,N,N);

aux_w = 1./(R(Er(aux)')-B).^2/Beta^2/h^2;
Lap_2r = sparse(aux,aux-2*Nh,-1/12*aux_w,N,N) + sparse(aux,aux-Nh,  4/3*aux_w,N,N) + ...
    sparse(aux,aux,      -5/2*aux_w,N,N) + sparse(aux,aux+Nh,  4/3*aux_w,N,N) + ...
    sparse(aux,aux+2*Nh,-1/12*aux_w,N,N);

% Radial component: gradient
%aux = 2*Nh+1:N-2*Nh;
aux_w = 1./(R(Er(aux)')-B)/Beta/h;
Grad_r = sparse(aux,aux-2*Nh,1/12*aux_w,N,N) + sparse(aux,aux-Nh,   -2/3*aux_w,N,N) + ...
    sparse(aux,aux+Nh,   2/3*aux_w,N,N) + sparse(aux,aux+2*Nh,-1/12*aux_w,N,N);
%-----------------------------------------------------------------------
%%Bdry top: centered
aux_w = (-1./(R(Er(bdry_top)')-B).^2 + 2./((R(Er(bdry_top)')-B).*R(Er(bdry_top)')))/Beta/h;
Lap_r_ghost_top = sparse(ghost_top,bdry_top+Nh, 0.5*aux_w,N,N) + ...
    sparse(ghost_top,bdry_top-Nh,-0.5*aux_w,N,N);

aux_w = 1./(R(Er(bdry_top)')-B).^2/Beta^2/h^2;
Lap_2r_ghost_top = sparse(ghost_top,bdry_top-Nh,aux_w,N,N) + sparse(ghost_top,bdry_top,-2*aux_w,N,N) +...
    sparse(ghost_top,bdry_top+Nh,aux_w,N,N);


aux_w = 1./(R(Er(bdry_top)')-B)/Beta/h;
Grad_r_ghost_top = sparse(ghost_top,bdry_top+Nh, 0.5*aux_w,N,N) + ...
    sparse(ghost_top,bdry_top-Nh,-0.5*aux_w,N,N);

%%Bdry top:
Dr_bdry_top = sparse(bdry_top,bdry_top, 1.5*aux_w,N,N) +...
    sparse(bdry_top,bdry_top-Nh,-2*aux_w,N,N) + ...
    sparse(bdry_top,bdry_top-2*Nh,0.5*aux_w,N,N);


% Dr_bdry_top = sparse(bdry_top,bdry_top+Nh, 0.5*aux_w,N,N) +...
%               sparse(bdry_top,bdry_top-Nh, -0.5*aux_w,N,N);
%-----------------------------------------------------------------------
%%Bdry bot: centered
aux_w = (-1./(R(Er(bdry_bot)')-B).^2 + 2./((R(Er(bdry_bot)')-B).*R(Er(bdry_bot)')))/Beta/h;
Lap_r_ghost_bot = sparse(ghost_bot,bdry_bot+Nh, 0.5*aux_w,N,N) + ...
    sparse(ghost_bot,bdry_bot-Nh,-0.5*aux_w,N,N);

aux_w = 1./(R(Er(bdry_bot)')-B).^2/Beta^2/h^2;
Lap_2r_ghost_bot = sparse(ghost_bot,bdry_bot-Nh,aux_w,N,N) + sparse(ghost_bot,bdry_bot,-2*aux_w,N,N) +...
    sparse(ghost_bot,bdry_bot+Nh,aux_w,N,N);


aux_w = 1./(R(Er(bdry_bot)')-B)/Beta/h;
Grad_r_ghost_bot = sparse(ghost_bot,bdry_bot+Nh, 0.5*aux_w,N,N)  + ...
    sparse(ghost_bot,bdry_bot-Nh,-0.5*aux_w,N,N);


%%Bdry bot:
% Dr_bdry_bot = sparse(bdry_bot,bdry_bot+Nh, 0.5*aux_w,N,N) +...
%               sparse(bdry_bot,bdry_bot-Nh, -0.5*aux_w,N,N);
Dr_bdry_bot = sparse(bdry_bot,bdry_bot, -1.5*aux_w,N,N) +...
    sparse(bdry_bot,bdry_bot+Nh, 2*aux_w,N,N) + ...
    sparse(bdry_bot,bdry_bot+2*Nh,-0.5*aux_w,N,N);

%
% %%Bdry bot:
% Bdry_bot = sparse(bdry_bot,bdry_bot, -1.5*aux_w,N,N) +...
%               sparse(bdry_bot,bdry_bot+Nh, 2*aux_w,N,N) + ...
%               sparse(bdry_bot,bdry_bot+2*Nh,-0.5*aux_w,N,N);
%-----------------------------------------------------------------------



%-----------------------------------------------------------------------
% Differentiation matrix: Laplacian
%-----------------------------------------------------------------------
Lap = Lap + (Lap_2r + Lap_r) +...
    (Lap_2r_ghost_bot + Lap_r_ghost_bot) +...
    (Lap_2r_ghost_top + Lap_r_ghost_top);
%-----------------------------------------------------------------------
% Differentiation matrix: Gradient (radial component)
%-----------------------------------------------------------------------
Grad_r = Grad_r + Grad_r_ghost_top + Grad_r_ghost_bot;
%-----------------------------------------------------------------------
% Differentiation matrix: Dr
%-----------------------------------------------------------------------
Dr =  Grad_r + Dr_bdry_top + Dr_bdry_bot;
%-----------------------------------------------------------------------
log_sig_lam = Dlam*log(sigma);
log_sig_the = Dthe*log(sigma);
log_sig_r   = Dr*log(sigma);

log_sig_lam(ghost_bot) = log_sig_lam(bdry_bot);
log_sig_the(ghost_bot) = log_sig_the(bdry_bot);
log_sig_r(ghost_bot)   = log_sig_r(bdry_bot);

log_sig_lam(ghost_top) = log_sig_lam(bdry_top);
log_sig_the(ghost_top) = log_sig_the(bdry_top);
log_sig_r(ghost_top)   = log_sig_r(bdry_top);
%-----------------------------------------------------------------------
D = Lap + sparse(1:N,1:N,log_sig_r,N,N)*Grad_r + ...
    sparse(1:N,1:N,log_sig_the,N,N)*Grad_the   + ...
    sparse(1:N,1:N,log_sig_lam,N,N)*Grad_lam   + ...
    Dr_bdry_bot + sparse(bdry_top,bdry_top,ones(Nh,1),N,N);
%-----------------------------------------------------------------------






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GEC SOLVER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

f = zeros(N,1);
f(bdry_bot) = -source./sigma(bdry_bot);

%--------------------------------------------------------------------------
% Iterative solver
%--------------------------------------------------------------------------

%Dirichlet BCs
aux = setdiff(1:N,bdry_top);
u = zeros(N,1);
u(bdry_top) = 0;
rhs = f(aux)-D(aux,bdry_top)*u(bdry_top);


%Reordering & precond. Diff. matrix
A = D(aux,aux);
o = symrcm(A);
[~,os] = sort(o);
Li = A(o,o);
[L,U] = ilu(Li);

tic, [ui,flag,relres,iter,resvec] = gmres(Li,rhs(o),5,1e-11,[],L,U); time = toc;
u(aux) = ui(os);

fprintf('done.\n') ;
disp('-----------------------------------------------------------')
disp(['Elapsed time is ',num2str(time),' seconds.'])
disp(['flag GMRES = ', num2str(flag),'; # iterations = ', num2str(iter),'; residual = ', num2str(relres),'.'])
disp('-----------------------------------------------------------')

figure(11), semilogy(resvec), 
xlabel('# iterations'), ylabel('residual')
drawnow

Er   =-Dr*u;
Jr   = sigma.*Er;
Jr_t = Jr(bdry_top);
I = xh(:,4)'*Jr_t*rf^2;


%--------------------------------------------------------------------------
% Interpolate solution
%--------------------------------------------------------------------------
F = scatteredInterpolant(x,y,z,Jr);

load(strcat('node_distribution/',NodeDistrib),'x','y','z','R')
[~,~,e] = cart2sph(x,y,z);
ind = find(R(e)>=rb);

Jr_s = zeros(length(x),1);
Jr_s(ind) = F(x(ind),y(ind),z(ind));
%--------------------------------------------------------------------------

%fprintf('done.\n') ;

return











