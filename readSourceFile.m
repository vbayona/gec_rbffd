function [tcu,ecu,lat,lon] = readSourceFile(coarsen,SourceFile)

%-----------------------------------------------------------------------
% Load source locations from file: ONLY provided from latitude (-40,40)
%
% INPUT:
%
%   coarsen - set to 0 for original data, 2 or 4 for coarsening (by sum)
%   SourceFile - name of the file containing the source
%
%
% OUTPUT:
%
%   tcu - thunderclouds, size: [lon,lat,month,UT]
%   ecu - electrified shower clouds, size: [lon,lat,month,UT]
%   lat - latitude of lower left corner of box
%   lon - longitude of lower left corner of box
%
%-----------------------------------------------------------------------

ncid = netcdf.open(SourceFile,'NOWRITE');
%-------------
varid = netcdf.inqVarID(ncid,'current');
current = netcdf.getVar(ncid,varid,'double');
tcu     = current;
ecu     = current;
%-------------
lon = -180:179;
lat = -90:89;

if coarsen
    
    step = coarsen;
    B = ones(step);
    lon = lon(1:step:end-1);
    lat = lat(1:step:end-1);
    tcu_new = zeros(size(tcu,1)/step,size(tcu,2)/step,size(tcu,4),size(tcu,3));
    ecu_new = zeros(size(tcu,1)/step,size(tcu,2)/step,size(tcu,4),size(tcu,3));
    for i = 1:size(tcu,3)
        for j = 1:size(tcu,4)
            temp = filter2(B,tcu(:,:,i,j),'same');
            tcu_new(:,:,j,i) = temp(step/2:step:end,step/2:step:end);
            
            temp = filter2(B,ecu(:,:,i,j),'same');
            ecu_new(:,:,j,i) = temp(step/2:step:end,step/2:step:end);
        end
    end
    tcu = tcu_new;
    ecu = ecu_new;
    
else
    
    tcu_new = zeros(size(tcu,1),size(tcu,2),size(tcu,4),size(tcu,3));
    ecu_new = zeros(size(tcu,1),size(tcu,2),size(tcu,4),size(tcu,3));
    for i = 1:size(tcu,3)
        for j = 1:size(tcu,4)
            tcu_new(:,:,j,i) = tcu(:,:,i,j);
            ecu_new(:,:,j,i) = ecu(:,:,i,j);
        end
    end
    tcu = tcu_new;
    ecu = ecu_new;
    
end

return